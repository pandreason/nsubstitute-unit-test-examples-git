﻿using System;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Contains event data associated with the BankAccountManager.SmsMessageSent event
    /// </summary>
    public class SmsMessageSentEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the SmsMessageSentEventArgs class
        /// </summary>
        /// <param name="message">SMS message that was sent</param>
        public SmsMessageSentEventArgs(string message)
        {
            Message = message;
        }

        /// <summary>
        /// Gets or sets the SMS message that was sent
        /// </summary>
        public string Message { get; private set; }
    }
}
