﻿using System;
using System.Threading.Tasks;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Represents an abstract bank account
    /// </summary>
    public abstract class BankAccountBase : IBankAccount
    {
        private readonly decimal _balance;
        private readonly string _accountNumber;

        #region Ctor
        
        /// <summary>
        /// Initializes a new instance of the BankAccountBase class with a zero
        /// balance, an empty string as the account number, and an empty string
        /// as the phone number.
        /// </summary>
        protected BankAccountBase()
        {
            _balance = 0m;
            _accountNumber = String.Empty;
            PhoneNumber = String.Empty;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the bank account number
        /// </summary>
        public string AccountNumber { get { return _accountNumber; } }

        /// <summary>
        /// Gets the bank account balance
        /// </summary>
        public decimal Balance { get { return _balance; } }

        /// <summary>
        /// Gets or sets the bank account owner's phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the bank account is overdrawn (i.e., negative balance)
        /// </summary>
        public abstract event EventHandler<OverdrawnEventArgs> Overdrawn;

        #endregion

        #region Methods

        /// <summary>
        /// Deposits a specified amount into the bank account.
        /// </summary>
        /// <param name="amount">Amount to deposit</param>
        public abstract void Deposit(decimal amount);

        /// <summary>
        /// Withdraws a specified amount from the bank account.
        /// </summary>
        /// <param name="amount">Amount to withdraw</param>
        public abstract void Withdraw(decimal amount);

        /// <summary>
        /// Gets whether the bank account has been suspended.
        /// </summary>
        /// <returns>True if the account is suspended; false otherwise</returns>
        public abstract bool IsAccountSuspended();

        /// <summary>
        /// Performs some validation on the account.
        /// </summary>
        public abstract void Validate();

        /// <summary>
        /// Determines the health of the account via some hueristics.
        /// </summary>
        /// <returns>Health metric for the account</returns>
        public abstract Task<double> ComputeAccountHealth();

        /// <summary>
        /// Computes the earned interest for this account based on a given rate and
        /// duration of time
        /// </summary>
        /// <param name="rate">Interest rate</param>
        /// <param name="numberOfYears">Number of years for accrual</param>
        /// <returns>Amount of interest given the conditions for interest</returns>
        public abstract decimal ComputeInterest(decimal rate, decimal numberOfYears);

        /// <summary>
        /// Gets whether the bank account is eligible for upgraded features.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsAccountEligibleForUpgrade()
        {
            return !IsAccountSuspended();
        }

        #endregion
    }
}
