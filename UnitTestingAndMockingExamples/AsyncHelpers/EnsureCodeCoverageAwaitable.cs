﻿using System.Threading.Tasks;

namespace UnitTestingAndMockingExamples.AsyncHelpers
{
    /// <summary>
    /// Wrapper for an asynchronous task that does not return a result
    /// </summary>
    public class EnsureCodeCoverageAwaitable
    {
        private readonly Task _task;

        public EnsureCodeCoverageAwaitable(Task task)
        {
            _task = task;
        }

        public EnsureCodeCoverageAwaiter GetAwaiter()
        {
            return new EnsureCodeCoverageAwaiter(_task);
        }
    }

    /// <summary>
    /// Wrapper for an asychronous task that returns a result
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnsureCodeCoverageAwaitable<T>
    {
        private readonly Task<T> _task;

        public EnsureCodeCoverageAwaitable(Task<T> task)
        {
            _task = task;
        }

        public EnsureCodeCoverageAwaiter<T> GetAwaiter()
        {
            return new EnsureCodeCoverageAwaiter<T>(_task);
        }
    }
}
