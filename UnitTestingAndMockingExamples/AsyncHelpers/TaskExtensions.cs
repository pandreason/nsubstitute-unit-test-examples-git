﻿using System.Threading.Tasks;

namespace UnitTestingAndMockingExamples.AsyncHelpers
{
    public static class TaskExtensions
    {
        public static EnsureCodeCoverageAwaitable EnsureCodeCoverage(this Task task)
        {
            return new EnsureCodeCoverageAwaitable(task);
        }

        public static EnsureCodeCoverageAwaitable<T> EnsureCodeCoverage<T>(this Task<T> task)
        {
            return new EnsureCodeCoverageAwaitable<T>(task);
        }
    }
}
