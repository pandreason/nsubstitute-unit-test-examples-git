﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace UnitTestingAndMockingExamples.AsyncHelpers
{
    /// <summary>
    /// Custom awaiter that simulates a task not being immediately completed (but only
    /// in DEBUG release). This awaiter is to be used with asynchronous methods
    /// that do not need to return a result.
    /// </summary>
    public class EnsureCodeCoverageAwaiter : INotifyCompletion
    {
        private TaskAwaiter _taskAwaiter;

#if DEBUG
        private bool _forceAsync;
#endif

        public EnsureCodeCoverageAwaiter(Task task)
        {
            _taskAwaiter = task.GetAwaiter();

#if DEBUG
            _forceAsync = true;
#endif
        }

        public bool IsCompleted
        {
            get
            {
#if DEBUG
                if (_forceAsync) return false;  // Simulates Task.Yield() by "not being ready"
#endif
                return _taskAwaiter.IsCompleted;
            }
        }

        public void GetResult()
        {
#if DEBUG
            _forceAsync = false;
#endif

            _taskAwaiter.GetResult();
        }

        public void OnCompleted(Action continuation)
        {
#if DEBUG
            _forceAsync = false;
#endif
            _taskAwaiter.OnCompleted(continuation);
        }
    }

    /// <summary>
    /// Custom awaiter that simulates a task not being immediately completed (but only
    /// in DEBUG release). This awaiter is to be used with asynchronous methods
    /// that need to return a result.
    /// </summary>
    public class EnsureCodeCoverageAwaiter<T> : INotifyCompletion
    {
        private TaskAwaiter<T> _taskAwaiter;

#if DEBUG
        private bool _forceAsync;
#endif

        public EnsureCodeCoverageAwaiter(Task<T> task)
        {
            _taskAwaiter = task.GetAwaiter();

#if DEBUG
            _forceAsync = true;
#endif
        }

        public bool IsCompleted
        {
            get
            {
#if DEBUG
                if (_forceAsync) return false; // Simulates Task.Yield() by "not being ready"
#endif
                return _taskAwaiter.IsCompleted;
            }
        }

        public T GetResult()
        {
#if DEBUG
            _forceAsync = false;
#endif

            return _taskAwaiter.GetResult();
        }

        public void OnCompleted(Action continuation)
        {
#if DEBUG
            _forceAsync = false;
#endif
            _taskAwaiter.OnCompleted(continuation);
        }
    }
}