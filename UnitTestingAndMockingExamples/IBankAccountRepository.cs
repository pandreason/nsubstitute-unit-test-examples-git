﻿using System;
using System.Collections.Generic;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Represents a bank account repository capable of retrieving/storing IBankAccounts.
    /// </summary>
    public interface IBankAccountRepository
    {
        /// <summary>
        /// Occurs when a repository-related error arises
        /// </summary>
        event EventHandler ErrorOccurred;

        /// <summary>
        /// Retrieves a bank account based on its account number
        /// </summary>
        /// <param name="accountNumber">Account number of the account to retrieve</param>
        /// <returns>IBankAccount instance corresponding to the requested account</returns>
        IBankAccount GetBankAccount(string accountNumber);

        /// <summary>
        /// Attempts to retrieve a bank account based on its account number
        /// </summary>
        /// <param name="accountNumber">Account number of the account to retrieve</param>
        /// <param name="account">IBankAccount instance retrieved if successful</param>
        /// <returns>true if the bank account could be retrieved; false otherwise</returns>
        bool TryAndGetBankAccount(string accountNumber, out IBankAccount account);

        /// <summary>
        /// Stores a given bank account
        /// </summary>
        /// <param name="account">IBankAccount instance to store</param>
        void SaveBankAccount(IBankAccount account);

        /// <summary>
        /// Retrieves all bank accounts
        /// </summary>
        /// <returns>IEnumerable instance containing instances of IBankAccount
        /// corresponding to all accounts in the system</returns>
        IEnumerable<IBankAccount> GetAllBankAccounts();

        /// <summary>
        /// Releases all resources associated with the specified account
        /// </summary>
        /// <param name="account">Account to dispose of</param>
        void DisposeOfAccount(ref IBankAccount account);
    }
}
