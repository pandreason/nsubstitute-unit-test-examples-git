﻿using System.Collections.Generic;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Represents a Short Message Service (SMS) sender
    /// </summary>
    public interface ISmsSender
    {
        /// <summary>
        /// Gets or sets the name of the SMS provider (e.g., Verizon Wireless)
        /// </summary>
        string Provider { get; set; }

        /// <summary>
        /// Sends a specified SMS message text to a specified phone number
        /// </summary>
        /// <param name="phoneNumber">Recipient's phone number</param>
        /// <param name="message">Message to be sent</param>
        void Send(string phoneNumber, string message);

        /// <summary>
        /// Sends a specified SMS message text to multiple specified phone numbers
        /// </summary>
        /// <param name="phoneNumbers">Recipient's phone numbers</param>
        /// <param name="message">Message to be sent</param>
        void SendToMultiple(IEnumerable<string> phoneNumbers, string message);
    }
}
