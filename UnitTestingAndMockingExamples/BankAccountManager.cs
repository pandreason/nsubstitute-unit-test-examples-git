﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnitTestingAndMockingExamples.AsyncHelpers;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Represents a business logic entity for manipulating IBankAccounts.
    /// </summary>
    public class BankAccountManager
    {
        private readonly IBankAccountRepository _accountRepo;
        private readonly ISmsSender _smsSender;

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the BankAccountManager class.
        /// </summary>
        /// <param name="accountRepo">IBankAccountRepository instance for retrieving/storing
        /// IBankAccounts</param>
        /// <param name="smsSender">ISmsSender instance for sending SMS messages</param>
        public BankAccountManager(IBankAccountRepository accountRepo, ISmsSender smsSender)
        {
            _accountRepo = accountRepo;
            _smsSender = smsSender;

            _accountRepo.ErrorOccurred += OnAccountRepositoryErrorOccurred;
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when an SMS message is sent
        /// </summary>
        public event EventHandler<SmsMessageSentEventArgs> SmsMessageSent;

        #endregion

        #region Methods

        /// <summary>
        /// Withdraws and deposits the specified amounts into the account represented by
        /// the specified account number.
        /// </summary>
        /// <param name="accountNumber">Account number representing the account to be
        /// manipulated</param>
        /// <param name="amounts">Amounts to process (positive = deposit, negative = withdrawal)</param>
        public void ProcessTransactionsOnAccount(string accountNumber, IList<decimal> amounts)
        {
            if (amounts.Count == 0)
                return;

            IBankAccount account;
            try
            {
                account = _accountRepo.GetBankAccount(accountNumber);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to retrieve account '" + accountNumber + "'", ex);
            }

            account.Overdrawn += OnAccountOverdrawn;

            account.Validate();

            ((List<decimal>) amounts).Sort();
            foreach (var amount in amounts)
            {
                if (amount > 0m)
                    account.Deposit(amount);
                else
                    account.Withdraw(amount);
            }

            account.Validate();

            account.Overdrawn -= OnAccountOverdrawn;
            
            _accountRepo.SaveBankAccount(account);
        }

        /// <summary>
        /// Contacts the bank account manager via an SMS message.
        /// </summary>
        /// <param name="message">Message to be sent</param>
        public void ContactBankManagerBySms(string message)
        {
            _smsSender.Send("bankmanager", message);
            FireSmsMessageSentEvent(new SmsMessageSentEventArgs("Sent to bank manager: " + message));
        }

        /// <summary>
        /// Gets whether the specified account has good credit based on a specified
        /// delegate capable of calculating a credit score.
        /// </summary>
        /// <param name="account">IBankAccount instance to be evaluated</param>
        /// <param name="accountFitnessEvaluator">Delegate capable of mapping the IBankAccount's
        /// balance into a credit score</param>
        /// <returns>True if the given bank account has a good credit score; false otherwise</returns>
        public bool IsAccountCreditGood(IBankAccount account, Func<decimal, int> accountFitnessEvaluator)
        {
            var score = accountFitnessEvaluator(account.Balance);
            return score > 700;
        }

        /// <summary>
        /// Sends balance information to the specified IBankAccount's owner via 
        /// an SMS message
        /// </summary>
        /// <param name="account">IBankAccount instance with the appropriate
        /// information</param>
        public void SendBalanceInfoBySms(IBankAccount account)
        {
            if (account.AccountNumber == null)
                throw new ArgumentNullException();

            var message = String.Format("Account '{0}' balance: {1:c} (sent via {2})", 
                account.AccountNumber, account.Balance, _smsSender.Provider);
            _smsSender.Send(account.PhoneNumber, message);
            FireSmsMessageSentEvent(new SmsMessageSentEventArgs(message));
        }

        /// <summary>
        /// Updates the account number for a specified bank account with a specified
        /// new phone number. An SMS message is sent to the new number to confirm the 
        /// change.
        /// </summary>
        /// <param name="account">IBankAccount to be updated</param>
        /// <param name="newNumber">New phone number</param>
        public void UpdateAccountPhoneNumber(IBankAccount account, string newNumber)
        {
            account.PhoneNumber = newNumber;
            const string message = "Your phone number is now updated!";
            _smsSender.Send(account.PhoneNumber, message);
            FireSmsMessageSentEvent(new SmsMessageSentEventArgs(message));
        }

        /// <summary>
        /// Returns the number of bank accounts in the repository that do not have
        /// phone numbers.
        /// </summary>
        /// <returns></returns>
        public virtual int GetNumberOfAccountsWithInvalidPhoneNumbers()
        {
            var allAccounts = _accountRepo.GetAllBankAccounts();
            return allAccounts.Count(a => !Regex.IsMatch(a.PhoneNumber,@"^\d{3}-\d{3}-\d{4}$"));
        }

        /// <summary>
        /// Computes the average account health score for all accounts in the repository
        /// </summary>
        /// <returns>Average health score</returns>
        public async Task<double> GetAverageAccountHealth()
        {
            await Task.Yield();

            var allAccounts = _accountRepo.GetAllBankAccounts();
            var healthMetrics = new List<double>();
            foreach (var account in allAccounts)
                healthMetrics.Add(await account.ComputeAccountHealth().EnsureCodeCoverage());

            return healthMetrics.Average();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Handles the IBankAccount.Overdrawn event by sending an SMS message regarding the event.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Arguments for the event</param>
        private void OnAccountOverdrawn(object sender, OverdrawnEventArgs e)
        {
            var message = "Account " + e.AccountNumber + " is overdrawn; triggering balance is " + e.Balance;
            _smsSender.Send(((IBankAccount) sender).PhoneNumber, message);
            FireSmsMessageSentEvent(new SmsMessageSentEventArgs(message)); 
        }

        /// <summary>
        /// Raises the SmsMessageSentEvent event with the specified arguments
        /// </summary>
        /// <param name="e">Arguments for the event</param>
        private void FireSmsMessageSentEvent(SmsMessageSentEventArgs e)
        {
            var handler = SmsMessageSent;
            if (handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Handles the IBankAccountRepository.RepositoryErrorOccurred event by sending 
        /// an SMS message regarding the event.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Arguments for the event</param>
        private void OnAccountRepositoryErrorOccurred(object sender, EventArgs e)
        {
            const string message = "There was a repository error!";
            _smsSender.Send("it-manager", message);
            FireSmsMessageSentEvent(new SmsMessageSentEventArgs(message));
        }

        #endregion
    }
}
