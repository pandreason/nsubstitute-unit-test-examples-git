﻿using System;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Contains event data associated with the IBankAccount.Overdrawn event
    /// </summary>
    public class OverdrawnEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the OverdrawnEventArgs class
        /// </summary>
        /// <param name="accountNumber">Number of the account that's overdrawn</param>
        /// <param name="balance">Balance of the overdrawn account</param>
        public OverdrawnEventArgs(string accountNumber, decimal balance)
        {
            AccountNumber = accountNumber;
            Balance = balance;
        }

        /// <summary>
        /// Gets or sets the account number for the event
        /// </summary>
        public string AccountNumber { get; private set; }

        /// <summary>
        /// Gets or sets the account balance for the event
        /// </summary>
        public decimal Balance { get; private set; }
    }
}
