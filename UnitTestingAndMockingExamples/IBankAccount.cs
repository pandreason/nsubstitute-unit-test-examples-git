﻿using System;
using System.Threading.Tasks;

namespace UnitTestingAndMockingExamples
{
    /// <summary>
    /// Represents a simple bank account
    /// </summary>
    public interface IBankAccount
    {
        /// <summary>
        /// Gets the bank account number
        /// </summary>
        string AccountNumber { get; }

        /// <summary>
        /// Gets the bank account balance
        /// </summary>
        decimal Balance { get; }

        /// <summary>
        /// Gets or sets the bank account owner's phone number
        /// </summary>
        string PhoneNumber { get; set; }

        /// <summary>
        /// Occurs when the bank account is overdrawn (i.e., negative balance)
        /// </summary>
        event EventHandler<OverdrawnEventArgs> Overdrawn;

        /// <summary>
        /// Deposits a specified amount into the bank account.
        /// </summary>
        /// <param name="amount">Amount to deposit</param>
        void Deposit(decimal amount);

        /// <summary>
        /// Withdraws a specified amount from the bank account.
        /// </summary>
        /// <param name="amount">Amount to withdraw</param>
        void Withdraw(decimal amount);

        /// <summary>
        /// Gets whether the bank account has been suspended.
        /// </summary>
        /// <returns>True if the account is suspended; false otherwise</returns>
        bool IsAccountSuspended();

        /// <summary>
        /// Performs some validation on the account.
        /// </summary>
        void Validate();

        /// <summary>
        /// Determines the health of the account via some hueristics.
        /// </summary>
        /// <returns>Health metric for the account</returns>
        Task<double> ComputeAccountHealth();

        /// <summary>
        /// Computes the earned interest for this account based on a given rate and
        /// duration of time
        /// </summary>
        /// <param name="rate">Interest rate</param>
        /// <param name="numberOfYears">Number of years for accrual</param>
        /// <returns>Amount of interest given the conditions for interest</returns>
        decimal ComputeInterest(decimal rate, decimal numberOfYears);
    }
}
