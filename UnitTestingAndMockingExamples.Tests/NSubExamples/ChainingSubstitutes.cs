﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class ChainingSubstitutes
    {
        /// <summary>
        /// Use Case: 
        /// You have an interface to fake, and that interface has other components you also need to fake.
        /// 
        /// Example:
        /// When getting an account from the fake repository, we want to also create a 
        /// fake balance for the account.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Creating_substitutes_on_the_fly()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount("123").Balance.Returns(100m);
            stubRepo.GetBankAccount("XYZ").Balance.Returns(-50m);

            Assert.AreEqual(100m, stubRepo.GetBankAccount("123").Balance);
            Assert.AreEqual(-50m, stubRepo.GetBankAccount("XYZ").Balance);

            
            
            // The following is more verbose, and accomplishes the same thing
            var stubRepoLongWay = Substitute.For<IBankAccountRepository>();
            
            var positiveBalanceAccount = Substitute.For<IBankAccount>();
            positiveBalanceAccount.Balance.Returns(100m);
            var negativeBalanceAccount = Substitute.For<IBankAccount>();
            negativeBalanceAccount.Balance.Returns(-50m);
            stubRepoLongWay.GetBankAccount("123").Returns(positiveBalanceAccount);
            stubRepoLongWay.GetBankAccount("XYZ").Returns(negativeBalanceAccount);

            Assert.AreEqual(100m, stubRepoLongWay.GetBankAccount("123").Balance);
            Assert.AreEqual(-50m, stubRepoLongWay.GetBankAccount("XYZ").Balance);
        }
    }
}
