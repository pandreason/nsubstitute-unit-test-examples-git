using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class UsingAsyncCalls
    {
        [TestMethod]
        public async Task With_Task_FromResult()
        {
            var stubFirstAccount = Substitute.For<IBankAccount>();
            stubFirstAccount.ComputeAccountHealth().Returns(Task.FromResult(0.75));

            var stubSecondAccount = Substitute.For<IBankAccount>();
            stubSecondAccount.ComputeAccountHealth().Returns(Task.FromResult(0.25));

            var stubRepository = Substitute.For<IBankAccountRepository>();
            stubRepository.GetAllBankAccounts().Returns(new List<IBankAccount> {stubFirstAccount, stubSecondAccount});

            var manager = new BankAccountManager(stubRepository, null);

            var observedHealth = await manager.GetAverageAccountHealth();

            Assert.AreEqual(0.5, observedHealth);
        }
    }
    // ReSharper restore InconsistentNaming
}
