﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class RaisingEvents
    {
        /// <summary>
        /// Use Case:
        /// You want to raise an event that uses the generic EventHandler delegate/handler.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void EventHandler_delegate_types()
        {
            var errorOccurred = false;

            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.ErrorOccurred += (sender, args) => errorOccurred = true;

            stubRepo.ErrorOccurred += Raise.Event();  // This works because ErrorOccurred has a delegate type of EventHandler

            Assert.IsTrue(errorOccurred);
        }

        /// <summary>
        /// Use Case:
        /// You want to raise an event that uses a custom event handler/delegate.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Custom_event_args()
        {
            var accountNumber = "";
            var balance = 0.0m;

            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.Overdrawn += (sender, args) =>
            {
                accountNumber = args.AccountNumber;
                balance = args.Balance;
            };

            stubAccount.Overdrawn += Raise.EventWith(new object(), new OverdrawnEventArgs("123ABC", -100m));

            Assert.AreEqual("123ABC", accountNumber);
            Assert.AreEqual(-100m, balance);
        }
    }
}
