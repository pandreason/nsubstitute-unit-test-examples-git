﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class ReturningValues
    {
        /// <summary>
        /// Use Case:
        /// You want the fake to return different values, some of which depend on some code running
        /// in a delegate.
        /// 
        /// Example:
        /// The first call to GetBankAccount() will return null. The second call will invoke a 
        /// delegate which throws an exception.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Using_callbacks_for_different_return_values()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount("").ReturnsForAnyArgs(x => null, x => { throw new Exception(); });

            Assert.IsNull(stubRepo.GetBankAccount("whatever"));

            try
            {
                stubRepo.GetBankAccount("whatever");
            }
            catch
            {
                return;
            }

            Assert.Fail("Exception not thrown for second call to GetBankAccount()");
        }

        /// <summary>
        /// Use Case:
        /// N/A -- just showing how the most recent return value will be used for subsequent calls
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Only_the_most_recent_Returns_value_is_used()
        {
            var stubAccount = Substitute.For<IBankAccount>();

            stubAccount.IsAccountSuspended().Returns(true);
            Assert.IsTrue(stubAccount.IsAccountSuspended());

            stubAccount.IsAccountSuspended().Returns(false);
            Assert.IsFalse(stubAccount.IsAccountSuspended());
            Assert.IsFalse(stubAccount.IsAccountSuspended()); // Still false
        }

        /// <summary>
        /// Use Case:
        /// Your fake method doesn't return a value, but instead uses an out parameter.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void With_out_parameters()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            var dummyAccount = Substitute.For<IBankAccount>();
            dummyAccount.Balance.Returns(1000m);

            IBankAccount accountReturned;
            stubRepo.TryAndGetBankAccount("123", out accountReturned)
                .Returns(args =>
                {
                    args[1] = dummyAccount;
                    return true;
                });

            var gotAccount = stubRepo.TryAndGetBankAccount("123", out accountReturned);

            Assert.IsTrue(gotAccount);
            Assert.AreEqual(1000m, accountReturned.Balance);
        }

        /// <summary>
        /// Use Case:
        /// Your fake method uses an ref parameter that you want to control.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void With_ref_parameters()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            var dummyAccount = Substitute.For<IBankAccount>();

            stubRepo.When(r => r.DisposeOfAccount(ref dummyAccount))
                .Do(args => { args[0] = null; });

            stubRepo.DisposeOfAccount(ref dummyAccount);

            Assert.IsNull(dummyAccount);
        }
    }
}
