﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class ThrowingExceptions
    {
        // The NSubstitute docs say to use the .Returns*(___) for non-void methods.
        // For void methods, use the When(___).Do(___) syntax.

        /// <summary>
        /// Use Case:
        /// You have a method that returns a value, but your fake should throw an exception instead.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void From_non_void_methods()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount("test").Returns(x => { throw new Exception(); });

            try
            {
                stubRepo.GetBankAccount("test");
            }
            catch
            {
                return;
            }

            Assert.Fail("Exception was never thrown from GetBankAccount()");
        }

        /// <summary>
        /// Use Case:
        /// You have a fake void method that should throw an exception when called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void From_void_methods()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.When(r => r.SaveBankAccount(null)).Do(x => { throw new Exception(); });

            try
            {
                stubRepo.SaveBankAccount(null);
            }
            catch
            {
                return;
            }

            Assert.Fail("Exception was never thrown from SaveBankAccount()");
        }
    }
}
