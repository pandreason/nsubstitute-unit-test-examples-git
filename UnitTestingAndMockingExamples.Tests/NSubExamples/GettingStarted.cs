﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class GettingStarted
    {
        /// <summary>
        /// Use Case:
        /// You want a fake method to return some value.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Having_a_method_return_a_value()
        {
            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.IsAccountSuspended().Returns(true);

            Assert.IsTrue(stubAccount.IsAccountSuspended());
        }

        /// <summary>
        /// Use Case:
        /// You want to ensure some method on your fake was called with an exact argument.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Checking_that_a_method_was_called()
        {
            var mockAccount = Substitute.For<IBankAccount>();
            
            mockAccount.Withdraw(100.0m);

            mockAccount.Received().Withdraw(100.0m);
        }

        /// <summary>
        /// Use Case:
        /// You want to ensure some method on your fake was never called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Checking_that_a_method_was_not_called()
        {
            var mockAccount = Substitute.For<IBankAccount>();

            mockAccount.Withdraw(100.0m);

            mockAccount.DidNotReceive().Withdraw(100000.0m);
        }

        /// <summary>
        /// Use Case:
        /// You want to set properties on a fake object (that has public setters).
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Affecting_property_getters()
        {
            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.Balance.Returns(100m);         // Note we need Returns() here because Balance has no setter
            stubAccount.PhoneNumber = "865-425-0555";

            Assert.AreEqual(100m, stubAccount.Balance);
            Assert.AreEqual("865-425-0555", stubAccount.PhoneNumber);
        }

        /// <summary>
        /// Use Case:
        /// You want to see if your fake method was called regardless of the arguments passed to it.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Handling_arbitrary_method_arguments()
        {
            var mockRepo = Substitute.For<IBankAccountRepository>();

            mockRepo.GetBankAccount("whatever");

            mockRepo.Received().GetBankAccount(Arg.Any<string>());
        }

        /// <summary>
        /// Use Case:
        /// You want to see if your fake method was called based on some restrictions on the arguments.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Limiting_arguments_with_logic()
        {
            var mockRepo = Substitute.For<IBankAccountRepository>();

            mockRepo.GetBankAccount("ACC0091148");

            mockRepo.Received().GetBankAccount(Arg.Is<string>(s => s.StartsWith("ACC")));
        }

        /// <summary>
        /// Use Case:
        /// You want to use the arguments passed into your fake so that you can control
        /// how the fake behaves going forward.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Accessing_the_arguments_via_the_substitute()
        {
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(args =>
            {
                var stubAccount = Substitute.For<IBankAccount>();
                stubAccount.AccountNumber.Returns((string) args[0]); // Set this stub's account number based on the caller
                return stubAccount;
            });

            var account = stubRepo.GetBankAccount("ABC123");

            Assert.AreEqual("ABC123", account.AccountNumber);
        }

        /// <summary>
        /// Use Case:
        /// You want your fake method to return different values depending on how it was called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Having_multiple_return_values_for_separate_calls()
        {
            var first = Substitute.For<IBankAccount>();
            first.AccountNumber.Returns("123");
            var second = Substitute.For<IBankAccount>();
            second.AccountNumber.Returns("ABC");
            var third = Substitute.For<IBankAccount>();
            third.AccountNumber.Returns("X!!");

            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(first, second, third);

            Assert.AreEqual("123", stubRepo.GetBankAccount("whatever").AccountNumber);
            Assert.AreEqual("ABC", stubRepo.GetBankAccount("whatever").AccountNumber);
            Assert.AreEqual("X!!", stubRepo.GetBankAccount("whatever").AccountNumber);
        }

        /// <summary>
        /// Use Case:
        /// You want to fire an event to test functionality that depends on that event being fired.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Raising_events()
        {
            var errorOccurred = false;

            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.ErrorOccurred += (sender, args) => errorOccurred = true;

            stubRepo.ErrorOccurred += Raise.Event();

            Assert.IsTrue(errorOccurred);
        }
    }
}
