﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class CreatingSubstitutes
    {
        /// <summary>
        /// Use Case:
        /// You have a class with an abstract method that you'd like to fake.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Abstract_classes_can_be_used()
        {
            var bankAccount = Substitute.For<BankAccountBase>();
            
            bankAccount.IsAccountSuspended().Returns(true);   // Abstract method
            
            // The following wouldn't work because BankAccountBase.Balance is neither virtual nor abstract...
            //bankAccount.Balance.Returns(100m);

            Assert.IsTrue(bankAccount.IsAccountSuspended());
            Assert.AreEqual(0m, bankAccount.Balance);
        }

        /// <summary>
        /// Use Case:
        /// You have a class with an abstract method that you'd like to fake, and you need
        /// to pass arguments to the concrete constructor.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Passing_ctor_arguments_to_classes_being_substituted()
        {
            var dummyRepo = Substitute.For<IBankAccountRepository>();
            var mockSmsSender = Substitute.For<ISmsSender>();
            
            // Making a substitute for a non-abstract class, but we're passing args to the ctor...
            var mockAccountManager = Substitute.For<BankAccountManager>(dummyRepo, mockSmsSender);

            // Using the real ContactBankManagerBySms() method implementation here...
            mockAccountManager.ContactBankManagerBySms("hello");  

            mockAccountManager.Received().ContactBankManagerBySms("hello");
            mockSmsSender.Received().Send("bankmanager", "hello");
        }

        /// <summary>
        /// Use Case:
        /// You want to assemble a single object that fakes multiple interfaces.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Implementing_multiple_interfaces_at_once()
        {
            var disposableRepo = Substitute.For<IBankAccountRepository, IDisposable>();
            
            // Note: See the weird casting you have to do. Intellisense picks up the first type you
            //       provide to For<>().

            // ReSharper disable SuspiciousTypeConversion.Global
            using ((IDisposable) disposableRepo)
            {
                disposableRepo.GetAllBankAccounts();
            }

            ((IDisposable) disposableRepo.Received()).Dispose();
            // ReSharper restore SuspiciousTypeConversion.Global
        }
    }
}
