﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class CheckingCallOrder
    {
        /// <summary>
        /// Use Case:
        /// You want to verify that methods are called in a specific order.
        /// 
        /// Example:
        /// The account manager is expected to process transactions in ascending sorted
        /// order, so withdrawals (negative values) should happen first, followed by 
        /// deposits.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Temporal_coupling_for_methods()
        {
            var mockAccount = Substitute.For<IBankAccount>();
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(mockAccount);

            var dummyManager = new BankAccountManager(stubRepo, Substitute.For<ISmsSender>());

            // Note the random order of the numbers in the list
            dummyManager.ProcessTransactionsOnAccount("whatever", new List<decimal> { 40m, -20m, 30m });

            Received.InOrder(() =>
            {
                mockAccount.Withdraw(-20m);
                mockAccount.Deposit(30m);
                mockAccount.Deposit(40m);
            });
        }

        /// <summary>
        /// Use Case:
        /// N/A -- just demonstrating the call order and expectation order don't matter here
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Order_does_not_matter_by_default()
        {
            var mockAccount = Substitute.For<IBankAccount>();

            mockAccount.Deposit(100m);
            mockAccount.Withdraw(-20m);

            // Note the opposite order of expectations here...
            mockAccount.Received().Withdraw(-20m);
            mockAccount.Received().Deposit(100m);
        }

        /// <summary>
        /// Use case:
        /// You have some calls that must be in order, but other calls on a fake interface may
        /// occur as well.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Mixing_ordered_and_unordered_expectations()
        {
            var mockAccount = Substitute.For<IBankAccount>();
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(mockAccount);

            var dummySuspendedResult = mockAccount.IsAccountSuspended(); // Calling this early on; should not affect ordered expectations

            var dummyManager = new BankAccountManager(stubRepo, Substitute.For<ISmsSender>());

            dummyManager.ProcessTransactionsOnAccount("whatever", new List<decimal> { -20m, 30m, 40m });

            Received.InOrder(() =>
            {
                mockAccount.Withdraw(-20m);
                mockAccount.Deposit(30m);
                mockAccount.Deposit(40m);
            });

            // This call happened before all the Withdraw/Deposit calls
            mockAccount.Received().IsAccountSuspended(); 
        }
    }
}
