﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class ExpectingCalls
    {
        /// <summary>
        /// Use Case:
        /// You want to see if your fake had a method called on it exactly once.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Called_once()
        {
            var mockSmsManager = Substitute.For<ISmsSender>();
            var dummyRepo = Substitute.For<IBankAccountRepository>();
            var dummyManager = new BankAccountManager(dummyRepo, mockSmsManager);

            dummyManager.ContactBankManagerBySms("hello?");

            mockSmsManager.Received().Send("bankmanager", "hello?");
        }

        /// <summary>
        /// Use Case:
        /// You want to see if your fake had a method called on it N times.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Expecting_a_certain_number_of_calls()
        {
            var mockAccount = Substitute.For<IBankAccount>();
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(mockAccount);
            
            var dummyManager = new BankAccountManager(stubRepo, Substitute.For<ISmsSender>());

            dummyManager.ProcessTransactionsOnAccount("whatever", new List<decimal> { 30m, 40m, 100m});

            // Note that any argument could be given...
            mockAccount.Received(3).Deposit(Arg.Any<decimal>());
        }

        /// <summary>
        /// Use Case:
        /// You want to ensure that a certain method on your fake was never called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Not_expecting_calls()
        {
            var mockAccount = Substitute.For<IBankAccount>();
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(mockAccount);

            var dummyManager = new BankAccountManager(stubRepo, Substitute.For<ISmsSender>());

            dummyManager.ProcessTransactionsOnAccount("whatever", new List<decimal> { 30m, 40m, 100m });

            mockAccount.DidNotReceive().Withdraw(Arg.Any<decimal>());
        }

        /// <summary>
        /// You want to see if a getter on your fake was ever called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Calls_to_getters()
        {
            var dummyManager = new BankAccountManager(Substitute.For<IBankAccountRepository>(), Substitute.For<ISmsSender>());
            var mockAccount = Substitute.For<IBankAccount>();
            var dummyEvaluator = new Func<decimal, int>(arg => 0);

            dummyManager.IsAccountCreditGood(mockAccount, dummyEvaluator);

            // ReSharper disable once UnusedVariable
            var ignored = mockAccount.Received().Balance;
        }

        /// <summary>
        /// Use Case:
        /// You want to see if a setter on your fake was ever called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Calls_to_setters()
        {
            var mockAccount = Substitute.For<IBankAccount>();

            mockAccount.PhoneNumber = "865-425-0555";

            // ReSharper disable UnusedVariable
            var ignored1 = mockAccount.Received().PhoneNumber = "865-425-0555";
            var ignored2 = mockAccount.Received().PhoneNumber = Arg.Any<string>();  // Generic "was setter called"
            // ReSharper restore UnusedVariable
        }

        /// <summary>
        /// Use Case:
        /// You want to reset the list of method/property calls to your fake.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Clearing_received_calls()
        {
            var mockAccount = Substitute.For<IBankAccount>();
            var stubRepo = Substitute.For<IBankAccountRepository>();
            stubRepo.GetBankAccount(Arg.Any<string>()).Returns(mockAccount);

            var dummyManager = new BankAccountManager(stubRepo, Substitute.For<ISmsSender>());

            dummyManager.ProcessTransactionsOnAccount("whatever", new List<decimal> { 30m, 40m, 100m });

            mockAccount.Received(3).Deposit(Arg.Any<decimal>());
            
            // Reset for another transaction
            mockAccount.ClearReceivedCalls();

            dummyManager.ProcessTransactionsOnAccount("whatever", new List<decimal> { -30m, -40m, 100m });

            mockAccount.Received(2).Withdraw(Arg.Any<decimal>());
            mockAccount.Received(1).Deposit(Arg.Any<decimal>());
        }

        /// <summary>
        /// Use Case:
        /// You want to see if method calls with specific argument constraints ever occurred.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Putting_conditions_on_instance_arguments()
        {
            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.AccountNumber.Returns("ABC123");

            var mockRepo = Substitute.For<IBankAccountRepository>();

            mockRepo.SaveBankAccount(stubAccount);

            mockRepo.Received().SaveBankAccount(Arg.Is<IBankAccount>(a => a.AccountNumber.StartsWith("ABC")));
        }

        /// <summary>
        /// Use Case:
        /// You want to see if method calls with an enumerable-type argument (which meets some constraint)
        /// ever occurred.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Putting_conditions_on_an_enumerable_argument()
        {
            var mockSms = Substitute.For<ISmsSender>();

            mockSms.SendToMultiple(new[] { "425-0555", "425-0556", "425-0557" }, "test");

            mockSms.Received().SendToMultiple(Arg.Is<IEnumerable<string>>(a => a.Count() == 3), "test");
        }
    }
}
