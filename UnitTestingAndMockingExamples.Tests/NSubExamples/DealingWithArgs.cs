﻿using NSubstitute;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class DealingWithArgs
    {
        /// <summary>
        /// Use Case:
        /// You want to fake a return value, but only if one of the arguments is a specific value.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Fixing_one_argument()
        {
            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.ComputeInterest(Arg.Any<decimal>(), 5).Returns(100);

            Assert.AreEqual(100, stubAccount.ComputeInterest(0.5m, 5));
            Assert.AreEqual(100, stubAccount.ComputeInterest(0.15m, 5));
            Assert.AreEqual(100, stubAccount.ComputeInterest(0.94m, 5));
            
            Assert.AreEqual(0, stubAccount.ComputeInterest(0.5m, 10)); // default(int) is zero
        }

        /// <summary>
        /// Use Case:
        /// You want to fake a return value, but only if an argument meets some condition.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Putting_logical_conditions_on_an_argment()
        {
            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.ComputeInterest(Arg.Is<decimal>(x => x < 0.2m), 5).Returns(100);

            Assert.AreEqual(100, stubAccount.ComputeInterest(0.05m, 5));
            Assert.AreEqual(100, stubAccount.ComputeInterest(0.15m, 5));
            
            Assert.AreEqual(0, stubAccount.ComputeInterest(0.94m, 5));  // default(decimal) is zero
        }

        /// <summary>
        /// Use Case:
        /// You don't care what arguments were given to your fake.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Ignoring_arguments()
        {
            // Method 1: open constraints
            var stubAccount = Substitute.For<IBankAccount>();
            stubAccount.ComputeInterest(Arg.Any<decimal>(), Arg.Any<decimal>()).Returns(100);

            Assert.AreEqual(100, stubAccount.ComputeInterest(0.5m, 5));
            Assert.AreEqual(100, stubAccount.ComputeInterest(0.15m, 15));

            
            // Method 2: dummy values + ReturnForAnyArgs()
            var otherStubAccount = Substitute.For<IBankAccount>();
            otherStubAccount.ComputeInterest(0, 0).ReturnsForAnyArgs(100);

            Assert.AreEqual(100, otherStubAccount.ComputeInterest(0.5m, 5));
            Assert.AreEqual(100, otherStubAccount.ComputeInterest(0.15m, 15));
        }
    }
}
