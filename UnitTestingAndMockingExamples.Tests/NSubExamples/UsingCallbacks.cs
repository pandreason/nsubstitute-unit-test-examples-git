﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class UsingCallbacks
    {
        // NOTE 1:
        // The NSubstitute docs say to use the .AndDoes(___) when also using .Returns*(___) for non-void methods.
        // For void methods, use the When(___).Do(___) syntax.
        //
        // NOTE 2:
        // All of the examples in the NSubstitute docs are another way of using
        // Received(). I'm currently struggling to think of a pressing reason why
        // the test code would need to perform additional logic after a substitute
        // call completed.

        /// <summary>
        /// Use Case:
        /// You want to perform some additional work after a non-void method on your fake was called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void For_non_void_methods()
        {
            var ratesGivenToMethod = new List<decimal>();

            var mockAccount = Substitute.For<IBankAccount>();
            mockAccount.ComputeInterest(0, 0)
                .ReturnsForAnyArgs(x => 0)
                .AndDoes(args => ratesGivenToMethod.Add((decimal) args[0]));

            mockAccount.ComputeInterest(0.05m, 1m);
            mockAccount.ComputeInterest(0.07m, 5m);

            var expectedList = new List<decimal> {0.05m, 0.07m};
            CollectionAssert.AreEqual(expectedList, ratesGivenToMethod);
        }

        /// <summary>
        /// Use Case:
        /// N/A -- just showing how argument specification can also take lambdas
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void An_alternative_that_uses_actions_with_arguments()
        {
            var ratesGivenToMethod = new List<decimal>();

            var mockAccount = Substitute.For<IBankAccount>();
            mockAccount.ComputeInterest(Arg.Do<decimal>(ratesGivenToMethod.Add), Arg.Any<decimal>()).Returns(0);

            mockAccount.ComputeInterest(0.05m, 1m);
            mockAccount.ComputeInterest(0.07m, 5m);

            var expectedList = new List<decimal> { 0.05m, 0.07m };
            CollectionAssert.AreEqual(expectedList, ratesGivenToMethod);
        }

        /// <summary>
        /// Use Case:
        /// You want to perform some additional work after a void method on your fake was called.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void For_void_methods()
        {
            var messagesSent = new List<string>();

            var mockSms = Substitute.For<ISmsSender>();
            mockSms.When(m => m.Send(Arg.Any<string>(), Arg.Any<string>()))
                .Do(args => messagesSent.Add((string) args[1]));

            mockSms.Send("425-0555", "hello?");
            mockSms.Send("452-0551", "test");

            var expectedMessages = new List<string> {"hello?", "test"};
            CollectionAssert.AreEqual(expectedMessages, messagesSent);
        }
    }
}
