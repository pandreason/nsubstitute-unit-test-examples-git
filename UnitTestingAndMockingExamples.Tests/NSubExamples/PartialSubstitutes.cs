﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace UnitTestingAndMockingExamples.Tests.NSubExamples
{
    [TestClass]
    public class PartialSubstitutes
    {
        /// <summary>
        /// Use Case:
        /// You're faking a virtual method and don't want the default implementation to
        /// be used.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Not_calling_the_method_being_overridden()
        {
            var partialStub = Substitute.ForPartsOf<BankAccountBase>();

            partialStub.When(x => x.IsAccountEligibleForUpgrade()).DoNotCallBase();
            partialStub.IsAccountSuspended().Returns(true);
            partialStub.IsAccountEligibleForUpgrade().Returns(true);
            
            // This should be true because DoNotCallBase() never allows the check for the
            // account being suspended to happen.
            Assert.IsTrue(partialStub.IsAccountEligibleForUpgrade());
        }

        /// <summary>
        /// Use Case:
        /// You're faking a virtual method and want the default implementation to
        /// be used.
        /// 
        /// NOTE:
        /// This is confusing and is the default behavior. Essentially your intent
        /// to override what the method returns is ignored.
        /// </summary>
        [TestMethod]
        [TestCategory("NSubstitute")]
        public void Also_calling_the_method_being_overridden()
        {
            var partialStub = Substitute.ForPartsOf<BankAccountBase>();

            partialStub.When(x => x.IsAccountEligibleForUpgrade());
            partialStub.IsAccountSuspended().Returns(true);
            partialStub.IsAccountEligibleForUpgrade().Returns(true);  // This is ignored because the virtual code runs instead (confusing!)

            Assert.IsFalse(partialStub.IsAccountEligibleForUpgrade());
        }
    }
}
