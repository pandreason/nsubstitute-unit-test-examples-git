﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class Delegates
    {
        /// <summary>
        /// Use Case:
        /// You want to verify the functionality of a method that takes a delegate.
        /// 
        /// Example:
        /// The BankAccountManager.IsAccountCreditGood() uses the strategy pattern by 
        /// allowing its caller to pass in a delegate that computes a score based on an
        /// IBankAccount's balance. This test verifies whether that score is correctly 
        /// converted to a true/false answer. The crux of this test is a stub 
        /// Func(decimal,int) that returns scores that we control.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.IsAccountCreditGood_KnownBalanceForGoodScore_ReturnsTrue()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Creating_a_stub_delegate()
        {
            // ***** ARRANGE *****
            var stubAccountRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var stubFitnessFunction = MockRepository.GenerateStub<Func<decimal, int>>();
            stubFitnessFunction
                .Stub(x => x(5000))
                .Return(750);

            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount
                .Stub(a => a.Balance)
                .Return(5000);

            var manager = new BankAccountManager(stubAccountRepo, stubSmsSender);

            // ***** ACT *****
            var hasGoodCredit = manager.IsAccountCreditGood(stubAccount, stubFitnessFunction);

            // ***** ASSERT *****
            Assert.IsTrue(hasGoodCredit);
        }
    }
}
