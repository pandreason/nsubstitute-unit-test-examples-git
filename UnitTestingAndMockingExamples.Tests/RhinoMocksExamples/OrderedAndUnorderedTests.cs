﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class OrderedAndUnorderedTests
    {
        /// <summary>
        /// Use Case:
        /// You want to verify that methods are called in a specific order.
        /// 
        /// Example:
        /// BankAccountManager.ProcessTransactionsOnAccount() is expected to use an
        /// instance of IBankAccountRepository to get an account and then save that
        /// account when finished processing. We don't care what happens in between
        /// the getting/saving, we just want to make sure that GetBankAccount() and
        /// SaveBankAccount() are called and in that order.
        /// 
        /// Note: It's best when using ordered expectations to use strict mocks.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_CallsGetBankAccountThenSaveBankAccount()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Verifying_that_methods_are_called_in_a_specific_order()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mocks = new MockRepository();
            var mockAccountRepository = mocks.StrictMock<IBankAccountRepository>();
            mockAccountRepository
                .Expect(r => r.ErrorOccurred += Arg<EventHandler>.Is.Anything);
            using (mocks.Ordered())
            {
                mockAccountRepository
                    .Expect(r => r.GetBankAccount(Arg<string>.Is.Anything))
                    .Return(stubAccount);
                mockAccountRepository
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Is.Anything));
            }
            mocks.ReplayAll();
            
            var acctManager = new BankAccountManager(mockAccountRepository, stubSmsSender);
            var amounts = new List<decimal> { 0 };
            
            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccountRepository.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// (None -- this is just an example of a Rhino Mocks behavior.)
        /// 
        /// Note: Ordered mocks work with dynamic mocks, but as long as that order
        /// exists at some point during the life of the mock, the test will pass.
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example of a Rhino Mocks behavior.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Why_strict_mocks_should_be_used_for_ordered_tests()
        {
            // ***** ARRANGE *****
            var mocks = new MockRepository();
            var acc = mocks.DynamicMock<IBankAccount>();
            using (mocks.Ordered())
            {
                acc
                    .Expect(a => a.Withdraw(Arg<decimal>.Is.Anything));
                acc
                    .Expect(a => a.Deposit(Arg<decimal>.Is.Anything));
            }
            mocks.ReplayAll();

            // ***** ACT *****
            acc.Deposit(100);   // Did deposit first, but the test will still pass.
            acc.Withdraw(-30);  // The following two lines represent the expected order, so the test passes.
            acc.Deposit(30);

            // ***** ASSERT *****
            acc.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// (Nothing additional -- this is just to show how Rhino Mocks behaves.)
        /// 
        /// Example:
        /// BankAccountManager.ProcessTransactionsOnAccount() is written to process
        /// all deposits first. This test sets up the transactions so the withdrawal
        /// is listed before the deposit. The code under test calls Deposit(30) and
        /// then Withdraw(-20). Our expectations have the order reversed, but it doesn't
        /// matter as long as Withdraw(-20) and Deposit(30) are called.
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mocks_are_unordered_by_default()
        {
            // ***** ARRANGE *****
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mockAccount = MockRepository.GenerateStrictMock<IBankAccount>();
            // By default, expectations are unordered.
            mockAccount
                .Expect(a => a.Withdraw(-20));
            mockAccount
                .Expect(a => a.Deposit(30));
            mockAccount
                .Expect(a => a.Overdrawn -= Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything);
            mockAccount
                .Expect(a => a.Overdrawn += Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything);
            mockAccount
                .Expect(a => a.Validate())
                .Repeat.Twice();

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(mockAccount);

            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);
            var amounts = new List<decimal> { -20, 30 };

            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case: 
        /// You want to (1) verify some methods are called in a certain order and
        /// (2) verify other methods are called regardless of order.
        /// 
        /// Example:
        /// BankAccountManager.ProcessTransactionsOnAccount() is written to process
        /// all deposits first. This test sets up the transactions so the withdrawal
        /// is listed before the deposit. We don't care what order Withdraw() and
        /// Deposit() are called, but we need to ensure those calls are bookended by
        /// GetBankAccount() and SaveBankAccount().
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_CallsGetThenHandlesDepositAndWithdrawalThenCallsSave()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mixing_ordered_and_unordered_mocks()
        {
            // ***** ARRANGE *****
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mocks = new MockRepository();
            var mockAccount = mocks.StrictMock<IBankAccount>();

            mockAccount
                .Expect(a => a.Overdrawn += Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything);
            using (mocks.Ordered())
            {
                mockAccount
                    .Expect(a => a.Validate());
                using (mocks.Unordered())
                {
                    mockAccount
                        .Expect(a => a.Withdraw(-20));  // Deposits happen first, but this section says we don't care
                    mockAccount
                        .Expect(a => a.Deposit(30));
                }
                mockAccount
                    .Expect(a => a.Validate());
            }
            mockAccount
                .Expect(a => a.Overdrawn -= Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything);
            mocks.ReplayAll();

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(mockAccount);

            var amounts = new List<decimal> { 30, -20 };
            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);

            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }
    }
}
