﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    // NOTES: 
    // (1) If using Constraints(), you cannot use Arg<T> for the parameters of expected methods.
    //     You can specify one or the other, but not both.
    //
    // (2) I didn't bother working through full end-to-end examples as with the other tests
    //     because I wanted to focus only on how to use constraints.
    [TestClass]
    public class Constraints
    {
        /// <summary>
        /// Use Case:
        /// You want to verify an argument (a simple type, or an object that implements IComparable)
        /// of a method call satisfies certain constraints.
        /// 
        /// Examples:
        ///   - Argument is any value
        ///   - Argument is a specific value
        ///   - Argument is not equal to a value
        ///   - Argument is greater than a value
        ///   - Argument is greater than or equal to a value
        ///   - Argument is less than a value
        ///   - Argument is less than or equal to a value
        ///   
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void A_comparable_method_argument_satisfies_given_constraints()
        {
            // I set these up as ordered mocks to ensure that the method calls below
            // the using-block trigger the correct expectations. The Repeat.Once()
            // is needed to make sure that each expectation is paired correctly
            // with the corresponding method call.

            var mockRepository = new MockRepository();
            var mockAccount = mockRepository.StrictMock<IBankAccount>();
            using (mockRepository.Ordered())
            {
                // Anything
                mockAccount
                    .Expect(a => a.Withdraw(Arg<decimal>.Is.Anything))
                    .Repeat.Once();
                
                // Anything (using IgnoreArguments)
                mockAccount
                    .Expect(a => a.Withdraw(0))
                    .IgnoreArguments()
                    .Repeat.Once();

                // Equal
                //acc.Expect(a => a.Withdraw(Arg<decimal>.Is.Equal((decimal) -20)));  // Don't do this. You have to cast.
                mockAccount
                    .Expect(a => a.Withdraw(-20))
                    .Repeat.Once();

                // Not equal
                mockAccount
                    .Expect(a => a.Withdraw(Arg<decimal>.Is.NotEqual((decimal)-20)))  // You have to cast
                    .Repeat.Once();  

                // Greater than
                mockAccount
                    .Expect(a => a.Deposit(Arg<decimal>.Is.GreaterThan((decimal)0)))  // You have to cast
                    .Repeat.Once();

                // Greater than or equal
                mockAccount
                    .Expect(a => a.Deposit(Arg<decimal>.Is.GreaterThanOrEqual((decimal)0)))  // You have to cast
                    .Repeat.Once(); 

                // Less than
                mockAccount
                    .Expect(a => a.Withdraw(Arg<decimal>.Is.LessThan((decimal)0)))  // You have to cast
                    .Repeat.Once();
                
                // Less than or equal
                mockAccount
                    .Expect(a => a.Withdraw(Arg<decimal>.Is.LessThanOrEqual((decimal)0)))  // You have to cast
                    .Repeat.Once();  
            }
            mockRepository.ReplayAll();

            mockAccount.Withdraw(100);  // Anything
            mockAccount.Withdraw(-30);  // IgnoreArguments
            mockAccount.Withdraw(-20);  // Equal to -20
            mockAccount.Withdraw(-30);  // Not equal to -20
            mockAccount.Deposit(20);    // Greater than 0
            mockAccount.Deposit(0);     // Greater than or equal to 0
            mockAccount.Withdraw(-20);  // Less than 0
            mockAccount.Withdraw(0);    // Less than or equal to 0

            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify an argument of a method call satisfies certain constraints.
        /// 
        /// Examples:
        ///   - Argument is null
        ///   - Argument is not null
        ///   - Argument is the same as another object
        ///   - Argument is different than another object
        ///   - Argument is of a specified type
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void An_instance_method_argument_satisfies_given_constraints()
        {
            // I set these up as ordered mocks to ensure that the method calls below
            // the using-block trigger the correct expectations. The Repeat.Once()
            // is needed to make sure that each expectation is paired correctly
            // with the corresponding method call.

            var mockRepository = new MockRepository();

            var mockAccountRepo = mockRepository.StrictMock<IBankAccountRepository>();
            
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            var stubAccount2 = MockRepository.GenerateStub<IBankAccount>();

            using (mockRepository.Ordered())
            {
                // Null
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Is.Null))
                    .Repeat.Once();

                // Not null
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Is.NotNull))
                    .Repeat.Once();

                // Is the same instance -- just use regular syntax
                //rep.Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Is.Same(stubAccount)));  // This works, but use the simpler way.
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(stubAccount))
                    .Repeat.Once();

                // Is not the same
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Is.NotSame(stubAccount)))
                    .Repeat.Once();

                // Is type of
                // NOTE: TypeOf refers to the <T> part of Arg. The documentation on the web is wrong.
                // There is no TypeOf(Type) or TypeOf<T>().
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Is.TypeOf));
            }
            mockRepository.ReplayAll();

            mockAccountRepo.SaveBankAccount(null);         // Null
            mockAccountRepo.SaveBankAccount(stubAccount);  // Not null
            mockAccountRepo.SaveBankAccount(stubAccount);  // Same instance
            mockAccountRepo.SaveBankAccount(stubAccount2); // Different instance
            mockAccountRepo.SaveBankAccount(stubAccount);  // Type of argument

            mockAccountRepo.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a string argument of a method call satisfies certain constraints.
        /// 
        /// Examples:
        ///   - Argument starts with a certain string
        ///   - Argument ends with a certain string
        ///   - Argument contains a certain string
        ///   - Argument matches a certain regular expression
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void A_string_method_argument_satisfies_given_constraints()
        {
            // I set these up as ordered mocks to ensure that the method calls below
            // the using-block trigger the correct expectations. The Repeat.Once()
            // is needed to make sure that each expectation is paired correctly
            // with the corresponding method call.

            var mockRepository = new MockRepository();

            var rep = mockRepository.StrictMock<IBankAccountRepository>();

            using (mockRepository.Ordered())
            {
                // Starts with
                rep
                    .Expect(r => r.GetBankAccount(null))
                    .IgnoreArguments()
                    .Constraints(Text.StartsWith("123"))
                    .Return(null)
                    .Repeat.Once();

                // Ends with
                rep
                    .Expect(r => r.GetBankAccount(null))
                    .IgnoreArguments()
                    .Constraints(Text.EndsWith("abc"))
                    .Return(null)
                    .Repeat.Once();

                // Contains
                rep
                    .Expect(r => r.GetBankAccount(null))
                    .IgnoreArguments()
                    .Constraints(Text.Contains("-04-"))
                    .Return(null)
                    .Repeat.Once();

                // Like
                rep
                    .Expect(r => r.GetBankAccount(null))
                    .IgnoreArguments()
                    .Constraints(Text.Like(@"^\d{3}\w{3}$"))
                    .Return(null)
                    .Repeat.Once();
            }
            mockRepository.ReplayAll();

            rep.GetBankAccount("123abc");      // Starts with "123"
            rep.GetBankAccount("123abc");      // Ends with "abc"
            rep.GetBankAccount("12-04-3abc");  // Contains "-04-"
            rep.GetBankAccount("123abc");      // Regex: exactly 3 numbers 3 letters

            rep.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a list (IEnumerable) argument of a method call satisfies
        /// certain constraints.
        /// 
        /// Examples:
        ///   - Argument contains at least the items in a certain list
        ///   - Argument is equal to a certain list (order matters)
        ///   - Argument has a specified member
        ///   - Argument's size/count is a certain value
        ///   - Argument's nth member satisfies some constraint
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void An_enumerable_method_argument_satisfies_given_constraints()
        {
            var mockRepository = new MockRepository();
            var mockSender = mockRepository.StrictMock<ISmsSender>();

            using (mockRepository.Ordered())
            {
                // Argument should contain all of the specified elements
                mockSender
                    .Expect(s => s.SendToMultiple(null, null))
                    .IgnoreArguments()
                    .Constraints(
                        List.ContainsAll(new[] { "425-0555", "425-0551" }), 
                        Is.Anything())
                    .Repeat.Once();

                // Argument should equal the specified elements (order is important)
                mockSender
                    .Expect(s => s.SendToMultiple(null, null))
                    .IgnoreArguments()
                    .Constraints(
                        List.Equal(new[] { "425-0555", "425-0551" }),
                        Is.Anything())
                    .Repeat.Once();

                // Argument should contain the specified object
                // NOTE: Personally, I'd prefer ContainsAll(), as it basically does the same thing.
                mockSender
                    .Expect(s => s.SendToMultiple(null, null))
                    .IgnoreArguments()
                    .Constraints(
                        List.IsIn("425-0555"),
                        Is.Anything())
                    .Repeat.Once();

                // Argument should contain a certain number of elements
                mockSender
                    .Expect(s => s.SendToMultiple(null, null))
                    .IgnoreArguments()
                    .Constraints(
                        List.Count(Is.GreaterThan(3)),
                        Is.Anything())
                    .Repeat.Once();

                // Argument should have an element that meets a constraint
                mockSender
                    .Expect(s => s.SendToMultiple(null, null))
                    .IgnoreArguments()
                    .Constraints(
                        List.Element(1, Text.Contains("-")),
                        Is.Anything())
                    .Repeat.Once();
            }
            mockRepository.ReplayAll();

            // ContainsAll
            mockSender.SendToMultiple(new[] { "813-1015", "425-0555", "813-1016", "425-0551" }, "whatever");
            // Equal
            mockSender.SendToMultiple(new[] { "425-0555", "425-0551" }, "whatever");
            // IsIn
            mockSender.SendToMultiple(new[] { "425-0555", "425-0551" }, "whatever");
            // Count is greater than 3
            mockSender.SendToMultiple(new[] { "813-1015", "425-0555", "813-1016", "425-0551" }, "whatever");
            // Element 1 contains a hyphe
            mockSender.SendToMultiple(new[] { "425-0555", "425-0551" }, "whatever");

            mockSender.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a property on an argument of a method call satisfies
        /// certain constraints.
        /// 
        /// Examples:
        ///   - Argument's property is a certain value
        ///   - Argument's property is within a certain range
        ///   - Combination of argument's properties meet certain constraints
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void A_property_of_a_method_argument_satisfies_given_constraints()
        {
            // NOTE: I personally recommend avoiding the Property.Value() and PropertyConstraint()
            // constructs because they rely on magic strings and they don't work with stubs
            // for some reason.

            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount.Stub(a => a.Balance).Return(20);
            stubAccount.PhoneNumber = "865-425-0555";
            
            var mockRepository = new MockRepository();
            var mockAccountRepo = mockRepository.StrictMock<IBankAccountRepository>();

            using (mockRepository.Ordered())
            {
                // Equality of an argument's property
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Matches(a => a.Balance == 20)))
                    .Repeat.Once();

                // More expressive boolean evaluation
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Matches(a => 
                        (a.Balance >= 10) &&
                        (a.Balance <= 30))))
                    .Repeat.Once();

                // Combining properties
                mockAccountRepo
                    .Expect(r => r.SaveBankAccount(Arg<IBankAccount>.Matches(a => 
                        (a.Balance > 0) && 
                        (a.PhoneNumber.StartsWith("865")))))
                    .Repeat.Once();
            }
            mockRepository.ReplayAll();

            mockAccountRepo.SaveBankAccount(stubAccount);  // Balance == 20
            mockAccountRepo.SaveBankAccount(stubAccount);  // Balance between 10 and 30
            mockAccountRepo.SaveBankAccount(stubAccount);  // Balance > 0 and phone number starts with 865

            mockAccountRepo.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify multiple arguments of a method call satisfy certain constraints.
        /// 
        /// Example:
        ///   - First argument meets one constraint, second argument meets another
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Multiple_arguments_of_a_method_satisfy_given_constraints()
        {
            // The Contraints() method takes n parameters, where n is the number of
            // arguments on the expected method call.

            var mockSender = MockRepository.GenerateStrictMock<ISmsSender>();
            mockSender
                .Expect(s => s.Send(null, null))
                .IgnoreArguments()
                .Constraints(
                    Text.StartsWith("865"),
                    Text.Contains("test"))
                .Repeat.Once();

            mockSender.Send("865-425-0555", "This is a test message");

            mockSender.VerifyAllExpectations();
        }
    }
}
