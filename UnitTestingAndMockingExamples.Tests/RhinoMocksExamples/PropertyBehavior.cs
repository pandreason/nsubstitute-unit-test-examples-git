﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class PropertyBehavior
    {
        /// <summary>
        /// Use Case:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        ///
        /// Example:
        /// ISmsSender has a Provider property that we'd like to set to make sure
        /// the BankAccountManager.SendBalanceInfoBySms() method works. With Rhino Mocks,
        /// when dealing with a *mock*, the compiler will let you set the property value,
        /// but it won't work as you expect.
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mock_with_a_normal_property_but_forgetting_to_use_PropertyBehavior()
        {
            // ***** ARRANGE *****
            var stubBankAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();

            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount
                .Stub(a => a.AccountNumber)
                .Return("123abc"); // Interface doesn't provide a public setter
            stubAccount
                .Stub(a => a.Balance)
                .Return(10);
            stubAccount.PhoneNumber = "865-425-0555"; 

            var mockSmsSender = MockRepository.GenerateMock<ISmsSender>();
            mockSmsSender.Provider = "VZW";   // Has no effect; Provider == null
            mockSmsSender
                .Expect(s => s.Send("865-425-0555", "Account '123abc' balance: $10.00 (sent via )"));
            //mockSmsSender.Expect(s => s.Send("865-425-0555", "Account '123abc' balance: $10.00 (sent via VZW)"));  // This would cause the test to fail

            var manager = new BankAccountManager(stubBankAccountRepository, mockSmsSender);

            // ***** ACT ******
            manager.SendBalanceInfoBySms(stubAccount);

            // ***** ASSERT *****
            mockSmsSender.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to mock an interface and set property values on that interface.
        ///
        /// Example:
        /// ISmsSender has a Provider property that we'd like to set to make sure
        /// the BankAccountManager.SendBalanceInfoBySms() method works. With Rhino Mocks,
        /// when dealing with a *mock*, the compiler will let you set the property value,
        /// but it won't work as you expect.
        /// 
        /// Note: PropertyBehavior() works on with Expect() and Stub().
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.SendBalanceInfoBySms_GivenValidInfo_SendsCorrectMessage()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mock_with_a_normal_property_using_PropertyBehavior()
        {
            // ***** ARRANGE *****
            var stubBankAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();

            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount
                .Stub(a => a.AccountNumber)
                .Return("123abc"); // Interface doesn't provide a public setter
            stubAccount
                .Stub(a => a.Balance)
                .Return(10);
            stubAccount.PhoneNumber = "865-425-0555";

            var mockSmsSender = MockRepository.GenerateMock<ISmsSender>();
            mockSmsSender
                .Stub(s => s.Provider)
                .PropertyBehavior();
            mockSmsSender
                .Provider = "VZW";   
            mockSmsSender
                .Expect(s => s.Send("865-425-0555", "Account '123abc' balance: $10.00 (sent via VZW)"));  // This would cause the test to fail

            var manager = new BankAccountManager(stubBankAccountRepository, mockSmsSender);

            // ***** ACT ******
            manager.SendBalanceInfoBySms(stubAccount);

            // ***** ASSERT *****
            mockSmsSender.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to stub an interface and set property values on that interface.
        ///
        /// Example:
        /// IBankAccount has a PhoneNumber property that we'd like to set to make sure
        /// the BankAccountManager.SendBalanceInfoBySms() method works. With Rhino Mocks,
        /// when dealing with a *stub*, property values can be set as expected.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.SendBalanceInfoBySms_GivenAccountWithPhoneNumber_UsesThatPhoneNumber()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Stub_with_a_normal_property_doesnt_need_PropertyBehavior()
        {
            // ***** ARRANGE *****
            var stubBankAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();

            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount
                .Stub(a => a.AccountNumber)
                .Return("123abc"); // Interface doesn't provide a public setter
            stubAccount
                .Stub(a => a.Balance)
                .Return(10);
            stubAccount.PhoneNumber = "865-425-0555";   // No need for PropertyBehavior() here

            var mockSmsSender = MockRepository.GenerateMock<ISmsSender>();
            mockSmsSender
                .Stub(s => s.Provider)
                .PropertyBehavior();
            mockSmsSender.Provider = "VZW";
            mockSmsSender
                .Expect(s => s.Send("865-425-0555", "Account '123abc' balance: $10.00 (sent via VZW)"));  // This would cause the test to fail

            var manager = new BankAccountManager(stubBankAccountRepository, mockSmsSender);

            // ***** ACT ******
            manager.SendBalanceInfoBySms(stubAccount);

            // ***** ASSERT *****
            mockSmsSender.VerifyAllExpectations();
        }
    }
}
