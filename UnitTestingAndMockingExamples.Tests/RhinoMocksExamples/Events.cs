﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class Events
    {
        /// <summary>
        /// Use Case:
        /// You want to verify that something subscribes to an event.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method subscribes to the
        /// IBankAccount.Overdrawn event so that further actions can be carried out if
        /// that event is raised while processing the transactions. This test verifies
        /// the event is subscribed to.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_SubscribesToOverdrawnEvent()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Verify_that_something_subscribes_to_an_event()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Expect(a => a.Overdrawn += Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything);

            var stubBankAccountRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            stubBankAccountRepo
                .Stub(r => r.GetBankAccount("12345"))
                .Return(mockAccount);

            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var manager = new BankAccountManager(stubBankAccountRepo, stubSmsSender);

            // ***** ACT *****
            manager.ProcessTransactionsOnAccount("12345", new List<decimal> { 0 });

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify that something unsubscribes from an event.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method subscribes to the
        /// IBankAccount.Overdrawn event so that further actions can be carried out if
        /// that event is raised while processing the transactions. Once done processing,
        /// the method should unsubscribe from the event. This test verifies the event is 
        /// unsubscribed from.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_UnsubscribesFromOverdrawnEvent()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Verify_that_something_unsubscribes_from_an_event()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Expect(a => a.Overdrawn -= Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything);
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mockBankAccountRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            mockBankAccountRepo
                .Stub(r => r.GetBankAccount("12345"))
                .Return(mockAccount);

            var manager = new BankAccountManager(mockBankAccountRepo, stubSmsSender);

            // ***** ACT *****
            manager.ProcessTransactionsOnAccount("12345", new List<decimal> { 0 });

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify an event was fired.
        /// 
        /// Example:
        /// BankAccountManager exposes a ContactBankManagerBySms() method which should
        /// fire the SmsMessageSent event when called. This test verifies that the
        /// SmsMessageSent event was fired.
        /// 
        /// Note: The only reason we need the isolation framework here is for 
        /// IBankAccountRepository and ISmsSender, as the code under test needs non-null 
        /// references.
        /// 
        /// Alternate Method Name:
        /// BankAccountManagerTests.ContactBankManagerBySms_FiresSmsMessageSentEvent()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Verify_that_an_event_was_raised()
        {
            // ***** ARRANGE *****
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            var stubRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            var manager = new BankAccountManager(stubRepository, stubSmsSender);
            
            bool smsWasSent = false;
            manager.SmsMessageSent += (o, e) => { smsWasSent = true; };

            // ***** ACT *****
            manager.ContactBankManagerBySms("Why is the vault empty?!");

            // ***** ASSERT *****
            Assert.IsTrue(smsWasSent);
        }

        /// <summary>
        /// Use Case:
        /// You want to fire an event to test functionality that depends on that event being fired.
        /// 
        /// Example:
        /// ISmsSender.Send() should be called when the IBankAccountRepository.ErrorOccurred
        /// event is fired. We're testing whether the constructor for BankAccountManager sends
        /// out an SMS message to the IT manager if the bank account repository fails.
        /// 
        /// Alternate Method Name:
        /// BankAccountManagerTests.RepositoryErrorOccurredEventFired_CallsSmsSend()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mock_the_raising_of_an_event()
        {
            // ***** ARRANGE *****
            var stubBankAcctRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            
            var mockSmsSender = MockRepository.GenerateMock<ISmsSender>();
            mockSmsSender
                .Expect(s => s.Send("it-manager", "There was a repository error!"));

            // ReSharper disable once UnusedVariable
            var manager = new BankAccountManager(stubBankAcctRepo, mockSmsSender);

            // ***** ACT *****
            // (We don't have a handler to actually wire up, so the handler is null.)
            stubBankAcctRepo.Raise(r => r.ErrorOccurred += null, this, new EventArgs());

            // ***** ASSERT *****
            mockSmsSender.VerifyAllExpectations();
        }
    }
}
