﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Collections.Generic;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class StrictVsDynamic
    {
        /// <summary>
        /// Use Case:
        /// (None -- this is an example of how Rhino Mocks works.)
        /// 
        /// By default MockRepository.GenerateMock() gives you a *dynamic* mock. A strict
        /// mock is one where you have to define the expectations of how that mock will be
        /// interacted with. If you expect a method to be called, you have to state that. 
        /// Any unexpected method calls will cause Rhino Mocks to throw an exception.
        /// 
        /// Pro: Quickly detects behavior you didn't plan for. For example, maybe you don't
        /// assume a method is being called, but through a strict mock find out about it
        /// and find that it's a bug.
        /// 
        /// Con: If the code under test is complex, tests with strict mocks may take several
        /// tries to get absolutely correct. Unless you have very specific behavior you need
        /// to lock down, tests with strict mocks can be brittle (i.e., less maintainable).
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is an example of how Rhino Mocks works.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        [ExpectedException(typeof(Rhino.Mocks.Exceptions.ExpectationViolationException))]
        public void Strict_mock_throws_an_exception_for_an_unexpected_method_call()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount
                .Expect(a => a.AccountNumber)
                .Return("123abc");

            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mockAccountRepository = MockRepository.GenerateStrictMock<IBankAccountRepository>();
            mockAccountRepository
                .Expect(r => r.ErrorOccurred += Arg<EventHandler>.Is.Anything);
            mockAccountRepository
                .Expect(r => r.GetBankAccount("123abc"))
                .Return(stubAccount);

            var manager = new BankAccountManager(mockAccountRepository, stubSmsSender);

            // ***** ACT *****
            manager.ProcessTransactionsOnAccount("123abc", new List<decimal> { 10, 20 });

            // ***** ASSERT *****
            // ProcessTransactionsOnAccount() calls SaveBankAccount(), for which we didn't set up an expectation.
            // Technically, the VerifyAllExpectations() doesn't even get called.
            mockAccountRepository.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// (None -- this is an example of how Rhino Mocks works.)
        /// 
        /// This test is the same as Strict_Mock_Throws_Exception_For_Unexpected_Method_Call(),
        /// but uses the default dynamic mock. Dynamic mocks don't care if methods were called
        /// that weren't expected.
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is an example of how Rhino Mocks works.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Dynamic_mock_doesnt_throw_an_exception_for_an_unexpected_method_call()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount
                .Expect(a => a.AccountNumber)
                .Return("123abc");
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mockAccountRepository = MockRepository.GenerateMock<IBankAccountRepository>();
            mockAccountRepository
                .Expect(r => r.ErrorOccurred += Arg<EventHandler>.Is.Anything);
            mockAccountRepository
                .Expect(r => r.GetBankAccount("123abc"))
                .Return(stubAccount);

            var manager = new BankAccountManager(mockAccountRepository, stubSmsSender);

            // ***** ACT *****
            manager.ProcessTransactionsOnAccount("123abc", new List<decimal> { 10, 20 });

            // ***** ASSERT *****
            // We didn't expect a call to SaveBankAccount(), but because this is a dynamic mock
            // there won't be an exception thrown.
            Assert.IsTrue(true);
            mockAccountRepository.VerifyAllExpectations();
        }
    }
}
