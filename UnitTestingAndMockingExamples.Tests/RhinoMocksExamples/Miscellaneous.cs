﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;
using Rhino.Mocks.Interfaces;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    /// <summary>
    /// These features of Rhino Mocks are presented here to provide examples of how to
    /// use them. I would suspsect that these features will be used rarely; however I wanted
    /// to provide working examples in case they are needed.
    /// 
    /// Please be wary of making your unit tests too complex by testing multiple pieces
    /// of logic.
    /// </summary>
    [TestClass]
    public class Miscellaneous
    {
        /// <summary>
        /// Use Case:
        /// You want to perform some additional action when a property or method is invoked.
        /// (This is essentially a callback that is invoked when the property or method is
        /// invoked.)
        /// 
        /// Example:
        /// Behind the scenes, 'foreach' relies on an instance of IEnumerable that exposes an
        /// enumerator that helps the loop iterate over the list via the GetEnumerator() method.
        /// If the code under test needs to go through that list multiple times, a "fresh" version
        /// of the enumerator needs to be provided each time GetEnumerator() is called.
        /// 
        /// More examples of WhenCalled() being used:
        ///   http://stackoverflow.com/questions/5310486/how-to-use-the-real-parameters-when-creating-a-stub-method-in-rhinomocks
        ///   http://stackoverflow.com/questions/1349364/rhino-mocks-set-a-property-if-a-method-is-called
        ///   http://stackoverflow.com/questions/5958108/can-i-define-implementation-of-methods-on-rhino-mocked-objects
        ///   http://stackoverflow.com/questions/3298871/can-i-set-up-a-mock-to-always-return-the-object-given-in-one-of-the-arguments
        /// 
        /// Alternative Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Doing_additional_work_once_a_stub_is_called()
        {
            // We want our mock list to return the enumerator for an actual list.
            // This implementation won't work because once we set up the expectation with
            // Rhino Mocks, it will always return the *same instance* given during the Stub()
            // setup because it's cached within Rhino Mocks.

            // ***** PROBLEM *****
            var realListForBadExample = new List<int> {1, 2, 3};

            var mockIListBadExample = MockRepository.GenerateStub<IList<int>>();
            mockIListBadExample
                .Stub(x => x.GetEnumerator())
                .Return(realListForBadExample.GetEnumerator())
                .Repeat.Any();

            // Go through the list once
            // ReSharper disable UnusedVariable
            foreach (var i in mockIListBadExample)
            {
            }
            // ReSharper restore UnusedVariable

            // Go through the list a second time
            var count = mockIListBadExample.Count();

            Assert.AreEqual(0, count);  // Assert we didn't go through the list a second time.


            // ***** SOLUTION *****

            // This approach works because there is no cached instance of the enumerator.
            // GetEnumerator() is re-called each time this part of the stub is used.
            var realList = new List<int> {1, 2, 3};

            var mockIList = MockRepository.GenerateStub<IList<int>>();
            mockIList
                .Stub(x => x.GetEnumerator())
                .WhenCalled(call => call.ReturnValue = realList.GetEnumerator())
                .Return(null) // is ignored, but needed for Rhinos validation
                .Repeat.Any();

            // Go through the list once
            // ReSharper disable UnusedVariable
            foreach (var i in mockIList)
            {
            }
            // ReSharper restore UnusedVariable

            // Try to go through the list a second time, but it won't work.
            count = 0;
            // ReSharper disable LoopCanBeConvertedToQuery
            // ReSharper disable UnusedVariable
            foreach (var i in mockIList)
                count++;
            // ReSharper restore UnusedVariable
            // ReSharper restore LoopCanBeConvertedToQuery

            Assert.AreEqual(3, count);  // Assert we did go through the list a second time.
        }

        /// <summary>
        /// Use Case:
        /// You want to provide custom behavior for a property or method.
        /// 
        /// Remarks:
        /// The likelihood of needing this is pretty low. Most of the providing a static
        /// return value via Return() should be sufficient. Basically the Do() handler
        /// let's you provide custom behavior, usually based on the method arguments. If
        /// you find yourself needing this capability, you should consider whether you
        /// are testing too many things in one test.
        /// 
        /// Alternative Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Performing_custom_behavior_for_a_property_or_method()
        {
            // ***** ARRANGE *****
            var stubRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubRepository
                .Stub(r => r.GetBankAccount(null))
                .IgnoreArguments()
                .Do(new GetBankAccountDelegate(BankAccountFactory));

            // ***** ACT *****
            var nullBankAccount = stubRepository.GetBankAccount("null");
            var overdrawnBankAccount = stubRepository.GetBankAccount("overdrawn");

            // ***** ASSERT *****
            Assert.IsNull(nullBankAccount);
            Assert.IsTrue(overdrawnBankAccount.Balance < 0);
        }

        /// <summary>
        /// Use Case:
        /// You have a concrete class (e.g., something not abstract or an interface) that
        /// you want to mock or stub. (This only works on virtual members.)
        /// 
        /// Alternative Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Stubbing_parts_of_a_concrete_class()
        {
            // ***** ARRANGE *****
            const int expectedNumInvalidPhoneNumbers = 28;

            var stubRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            stubRepo
                .Stub(r => r.GetAllBankAccounts())
                .Return(new List<IBankAccount>());

            var stubManager = MockRepository.GenerateStub<BankAccountManager>(stubRepo, null);
            stubManager
                .Stub(m => m.GetNumberOfAccountsWithInvalidPhoneNumbers())
                .Return(expectedNumInvalidPhoneNumbers);

            // ***** ACT *****
            var actualNumInvalidPhoneNumber = stubManager.GetNumberOfAccountsWithInvalidPhoneNumbers();

            // ***** ASSERT *****
            Assert.AreEqual(expectedNumInvalidPhoneNumbers, actualNumInvalidPhoneNumber);
        }

        /// <summary>
        /// Use Case:
        /// You have a stub method that uses an 'out' argument.
        /// 
        /// Example:
        /// IBankAccountRepository.TryAndGetBankAccount() returns the actual IBankAccount
        /// instance via an 'out' argument. Rhino Mocks can be set up to control what value
        /// is returned via that argument.
        /// 
        /// Alternative Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Stubbing_when_theres_an_out_argument()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount.PhoneNumber = "865-425-0555";
            
            var stubRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            stubRepo
                .Stub(r => r.TryAndGetBankAccount(
                    Arg<string>.Is.Anything, 
                    out Arg<IBankAccount>.Out(stubAccount).Dummy))
                .Return(true);

            // ***** ACT *****
            IBankAccount actualAccount;
            var actualResult = stubRepo.TryAndGetBankAccount("whatever", out actualAccount);

            // ***** ASSERT *****
            Assert.IsTrue(actualResult);
            Assert.AreEqual((object) stubAccount.PhoneNumber, actualAccount.PhoneNumber);
        }

        /// <summary>
        /// Use Case:
        /// You have a stub/mock method that uses a 'ref' argument.
        /// 
        /// Example:
        /// IBankAccountRepository.DisposeOfAccount() has a 'ref' argument that allows
        /// the reference to be set to null. Rhino Mocks can be set up to control what
        /// value the argument is set to.
        /// 
        /// Alternative Test Method Name:
        /// (None -- this is just an example showing how Rhino Mocks behaves.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Stubbing_when_theres_a_ref_argument()
        {
            // ***** ARRANGE *****
            var stubRepo = MockRepository.GenerateStub<IBankAccountRepository>();
            stubRepo
                .Stub(r => r.DisposeOfAccount(ref Arg<IBankAccount>.Ref(Is.Anything(), null).Dummy));

            var stubAccount = MockRepository.GenerateStub<IBankAccount>();

            // ***** ACT *****
            stubRepo.DisposeOfAccount(ref stubAccount);

            // ***** ASSERT *****
            Assert.IsNull(stubAccount);
        }

        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Stubbing_a_virtual_method_and_using_that_methods_body()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<BankAccountBase>();
            stubAccount.Stub(a => a.IsAccountSuspended()).Return(true);
            stubAccount.Stub(a => a.IsAccountEligibleForUpgrade())
                .CallOriginalMethod(OriginalCallOptions.NoExpectation)
                .Return(true); // Doesn't matter because original method is being used

            Assert.IsFalse(stubAccount.IsAccountEligibleForUpgrade());
        }

        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Overriding_a_virtual_method()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<BankAccountBase>();
            stubAccount.Stub(a => a.IsAccountSuspended()).Return(true);
            stubAccount.Stub(a => a.IsAccountEligibleForUpgrade()).Return(true); 

            Assert.IsTrue(stubAccount.IsAccountEligibleForUpgrade());
        }

        #region Helpers for Mocks_Do_Handler

        private delegate IBankAccount GetBankAccountDelegate(string accountNumber);

        private static IBankAccount BankAccountFactory(string accountNumber)
        {
            switch (accountNumber)
            {
                case "null" : 
                    return null;

                case "overdrawn":
                    var toReturn = MockRepository.GenerateStub<IBankAccount>();
                    toReturn.Stub(a => a.Balance).Return(-100);
                    return toReturn;
            }

            return null;
        }

        #endregion
    }
}
