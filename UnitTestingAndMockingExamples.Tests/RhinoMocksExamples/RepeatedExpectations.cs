﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class RepeatedExpectations
    {
        // Expectations in my opinion are more elegant than AssertWasCalled() and
        // AssertWasNotCalled(). Use Repeat.AtLeastOnce() and Repeat.Never() instead.
        //
        // Repeat.Any() doesn't make much sense to me. It means the method may be called
        // zero or more times. I think Repeat.Any() is a throwback to when strict mocks
        // were the norm. The new AAA-style syntax support seems to make that unnecessary.
        // Just use Stub() instead.
        //
        // Reference: Stack Overflow article about the Repeat construct...
        // http://stackoverflow.com/questions/887245/rhino-mocks-repeat-once-not-working

        /// <summary>
        /// Use Case:
        /// You want to verify a method is called one time or two times.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method is given two
        /// positive numbers and one negative number, meaning there should be two calls
        /// to Deposit() and one call to Withdraw().
        /// 
        /// Note: You need to use strict mocks.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_GiveTwoPosOneNeg_CallsWithdrawOnceAndDepositTwice()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Repeating_an_expectation_using_a_strict_mock()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateStrictMock<IBankAccount>();
            mockAccount
                .Expect(a => a.Overdrawn += Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything); // Need because of strict mock
            mockAccount
                .Expect(a => a.Overdrawn -= Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything); // Need because of strict mock
            mockAccount
                .Expect(a => a.Withdraw(Arg<decimal>.Is.Anything))
                .Repeat.Once();
            mockAccount
                .Expect(a => a.Deposit(Arg<decimal>.Is.Anything))
                .Repeat.Twice();
            mockAccount
                .Expect(a => a.Validate())
                .Repeat.Twice();

            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            
            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(mockAccount);

            var amounts = new List<decimal> { -20, 30, 50 };

            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);

            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();  
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a method is called one time or two times.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method is given two
        /// positive numbers and one negative number, meaning there should be two calls
        /// to Deposit() and one call to Withdraw().
        /// 
        /// Note: This example uses a *dynamic* mock.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_GiveTwoPosOneNeg_CallsWithdrawOnceAndDepositTwice()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Repeating_an_expectation_using_a_dynamic_mock()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Expect(a => a.Withdraw(Arg<decimal>.Is.Anything))
                .Repeat.Once();
            // Second call to Withdraw() should throw an exception.
            mockAccount
                .Stub(a => a.Withdraw(Arg<decimal>.Is.Anything))
                .Throw(new InvalidOperationException("Withdraw called more than once"));
            mockAccount
                .Expect(a => a.Deposit(Arg<decimal>.Is.Anything))
                .Repeat.Twice();
            // Third call to Deposit() should throw an exception
            mockAccount
                .Stub(a => a.Deposit(Arg<decimal>.Is.Anything))
                .Throw(new InvalidOperationException("Deposit called more than twice"));

            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            
            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(mockAccount);

            var amounts = new List<decimal> { -20, 30, 50 };

            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);

            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations(); 
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a method is called at least once or not at all.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method is given four
        /// positive numbers, meaning there should be at least one call to Deposit() and no 
        /// calls to Withdraw().
        /// 
        /// Note: If using a strict mock (instead of the dynamic mock returned by 
        /// GenerateMock(T)), any call to Withdraw() would throw an exception because that
        /// call wasn't expected.
        ///
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_GiveFourPos_CallsDepositAtLeastOnceAndNeverWithdraw()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Repeating_an_expectation_at_least_once_and_never()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateStrictMock<IBankAccount>();
            mockAccount
                .Expect(a => a.Overdrawn += Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything); // Need because of strict mock
            mockAccount
                .Expect(a => a.Overdrawn -= Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything); // Need because of strict mock
            mockAccount
                .Expect(a => a.Validate())
                .Repeat.Twice();
            mockAccount
                .Expect(a => a.Deposit(Arg<decimal>.Is.Anything))
                .Repeat.AtLeastOnce();
            mockAccount
                .Expect(a => a.Withdraw(Arg<decimal>.Is.Anything))
                .Repeat.Never(); // Technically this is redundant

            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(mockAccount);

            var amounts = new List<decimal> { 20, 30, 40, 50 };
            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);

            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a method is called at a specific number of times.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method is given four
        /// positive numbers, meaning there should be four calls to Deposit().
        /// 
        /// Notes:
        /// (1) Rhino Mocks doesn't give anything higher than Twice(), so use Times() to expect
        /// something to be called a specific number of times.
        /// (2) Not shown here because there's not really a good use case in this example is
        /// Times(int min, int max) to expect a method to be called between min and max number
        /// of times.
        ///
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_GiveFourPos_CallsDepositFourTimes()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Repeating_an_expectation_a_certain_number_of_times()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateStrictMock<IBankAccount>();
            mockAccount
                .Expect(a => a.Overdrawn += Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything); // Need because of strict mock
            mockAccount
                .Expect(a => a.Overdrawn -= Arg<EventHandler<OverdrawnEventArgs>>.Is.Anything); // Need because of strict mock
            mockAccount
                .Expect(a => a.Deposit(Arg<decimal>.Is.Anything))
                .Repeat.Times(4);
            mockAccount
                .Expect(a => a.Validate())
                .Repeat.Twice();
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(mockAccount);

            var amounts = new List<decimal> { 20, 30, 40, 50 };
            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);
                        
            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", amounts);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to control what stubs return on the first call.
        /// 
        /// Example:
        /// The setup for this test is that IBankAccountRepository.GetBankAccount() should
        /// return a specific value only the first time the method is called. Any 
        /// subsequent calls return default(T), which in this case is null.
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example of a Rhino Mocks feature.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Repeating_expectation_when_using_stubs()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();

            var stubRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(stubAccount)
                .Repeat.Once();

            // ***** ACT *****
            var firstCall = stubRepository.GetBankAccount("whatever");
            var secondCall = stubRepository.GetBankAccount("whatever");

            // ***** ASSERT *****
            Assert.AreEqual(stubAccount, firstCall);
            Assert.AreEqual(null, secondCall);
        }

        /// <summary>
        /// Use Case:
        /// You want to control what stubs return in a specific order.
        /// 
        /// Example:
        /// The setup for this test is that IBankAccountRepository.GetBankAccount() should
        /// return a specific value the first two times and then a second value the third
        /// time. Any subsequent calls return default(T), which in this case is null.
        /// 
        /// Note: Although expectations on stubs/mocks are unordered by default, the order 
        /// in which you set up the *return* values matters.
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example of a Rhino Mocks feature.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Controlling_what_stubs_return_in_a_specific_order()
        {
            // ***** ARRANGE *****
            var firstStubAccount = MockRepository.GenerateStub<IBankAccount>();
            var secondStubAccount = MockRepository.GenerateStub<IBankAccount>();
            
            var stubRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(firstStubAccount)
                .Repeat.Twice();
            stubRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(secondStubAccount)
                .Repeat.Once();

            // ***** ACT *****
            var actualValues = new IBankAccount[4];
            for (var i = 0; i < 4; i++)
                actualValues[i] = stubRepository.GetBankAccount("whatever");

            // ***** ASSERT *****
            var expectedValues = new[] { firstStubAccount, firstStubAccount, secondStubAccount, null };
            CollectionAssert.AreEqual(expectedValues, actualValues);
        }
    }
}
