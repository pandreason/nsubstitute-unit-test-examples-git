﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class Properties
    {
        // See also -- PropertyBehavior, RepeatedExpectations

        
        /// <summary>
        /// Use Case:
        /// You want to mock an interface where one property getter should be called
        /// but another property getter that will be called doesn't require an expectation.
        ///
        /// Example:
        /// BankAccountManager.SendBalanceInfoBySms() can only function correctly if it
        /// queries the given IBankAccount to obtain the phone number. It also happens to
        /// query the AccountNumber property. If you don't specify what value a property has, 
        /// Rhino Mocks uses default(T). In this case the method under test would throw
        /// an exception because it doesn't like nulls.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.SendBalanceInfoBySms_GivenAccountWithPhoneNumber_UsesThatPhoneNumber()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mock_getter_is_called_but_no_expectation_is_needed_for_the_getter()
        {
            // ***** ARRANGE *****
            var stubBankAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Stub(a => a.AccountNumber)
                .Return(String.Empty); // Without this, code-under-test throws an exception
            mockAccount
                .Expect(a => a.PhoneNumber)
                .Return("whatever");

            var manager = new BankAccountManager(stubBankAccountRepository, stubSmsSender);
            
            // ***** ACT ******
            manager.SendBalanceInfoBySms(mockAccount);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to mock an interface and verify that a getter for a property is called.
        ///
        /// Example:
        /// BankAccountManager.SendBalanceInfoBySms() can only function correctly if it
        /// queries the given IBankAccount to obtain the phone number. This test verifies
        /// the getter on the mock property was called.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.SendBalanceInfoBySms_GivenAccountWithAccountNumber_UsesThatAccountNumber()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Verifying_that_a_getter_was_called()
        {
            // ***** ARRANGE *****
            var stubBankAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Expect(a => a.AccountNumber)
                .Return("number");

            var manager = new BankAccountManager(stubBankAccountRepository, stubSmsSender);

            // ***** ACT ******
            manager.SendBalanceInfoBySms(mockAccount);

            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to mock an interface and set property values on that interface.
        ///
        /// Example:
        /// IBankAccount has a PhoneNumber property that we'd like to set to make sure
        /// the BankAccountManager.SendBalanceInfoBySms() method works. With Rhino Mocks,
        /// when dealing with a *mock*, the compiler will let you set the property value,
        /// but it won't work as you expect.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.UpdateAccountPhoneNumber_GivenNewPhoneNumber_SetsNewAccountPhoneNumber()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Verifying_that_a_setter_is_called()
        {
            // ***** ARRANGE *****
            var stubBankAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Expect(a => a.PhoneNumber = "865-425-0555");

            var manager = new BankAccountManager(stubBankAccountRepository, stubSmsSender);

            // ***** ACT ******
            manager.UpdateAccountPhoneNumber(mockAccount, "865-425-0555");
            
            // ***** ASSERT *****
            mockAccount.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify a setter is being called but you want to override what the
        /// getter returns (i.e., ignore the set operation).
        /// 
        /// Example:
        /// IBankAccount has a PhoneNumber property that we'd like to make sure gets set
        /// to something. We'd like to ignore the actual setting -- we just want to make
        /// sure it happens. To do this, make the getter a stub.
        /// 
        /// Alternate Test Method Name:
        /// (None -- this is just an example of how to use Rhino Mocks.)
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Expecting_a_setter_to_be_called_but_having_the_getter_return_some_other_value()
        {
            // ***** ARRANGE *****
            var mockAccount = MockRepository.GenerateMock<IBankAccount>();
            mockAccount
                .Stub(a => a.PhoneNumber)
                .Return("865-425-0555");
            mockAccount
                .Expect(a => a.PhoneNumber = Arg<string>.Is.Anything);

            // ***** ACT *****
            mockAccount.PhoneNumber = "865-813-1015";

            // ***** ASSERT *****
            Assert.AreEqual("865-425-0555", mockAccount.PhoneNumber);
            mockAccount.VerifyAllExpectations();
        }
    }
}
