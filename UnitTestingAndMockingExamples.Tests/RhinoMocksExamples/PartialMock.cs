﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class PartialMock
    {
        // Partial mocks allow you to keep the default behavior/implementation for a class
        // unless you provide other stubs/expectations for parts of that class. With a full
        // mock, Rhino Mocks controls the entire mock's implementation. With a partial mock,
        // Rhino Mocks only controls what you tell it to (e.g., expecting a method to return
        // a value you control).

        /// <summary>
        /// Use Case:
        /// You want to test the logic of a virtual method that calls an abstract method.
        /// 
        /// Example:
        /// The abstract class BankAccountBase has a virutal IsAccountEligibleForUpgrade()
        /// method that relies on an abstract IsAccountSuspended() method. To test the 
        /// virtual method we need to make the abstract method return a value we control.
        /// 
        /// Note: There needs to be a parameterless constructor for the base class.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountBaseTests.IsAccountEligibleForUpgrade_AccountIsSuspended_ReturnsFalse()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Using_a_partial_mock_to_support_mocking_abstract_methods()
        {
            // ***** ARRANGE *****
            var mockBankAccount = MockRepository.GeneratePartialMock<BankAccountBase>();
            mockBankAccount
                .Expect(p => p.IsAccountSuspended())
                .Return(true);

            // ***** ACT *****
            var canUpgrade = mockBankAccount.IsAccountEligibleForUpgrade();

            // ***** ASSERT *****
            Assert.IsFalse(canUpgrade);
            mockBankAccount.VerifyAllExpectations();
        }
    }
}
