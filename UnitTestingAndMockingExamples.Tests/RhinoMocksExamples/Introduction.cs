﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class Introduction
    {
        /// <summary>
        /// Use Case:
        /// You want to mock something on a interface and the code under test uses other interfaces.
        /// 
        /// Example:
        /// The BankAccountManager.SendBalanceInfoBySms() method uses the ISmsSender dependency,
        /// but relies on an IAccount instance. Also to get the BankAccountManager in place, we'll
        /// need to provide a dummy IBankAccountRepository.
        /// 
        /// Note: Remember that mocks test behavior. Stubs help set up other dependencies to help
        /// isolate the behavior your mock is trying to test.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.SendBalanceInfoBySms_GivenSpecificAccount_SendsMessageWithCorrectContent()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Passing_mocks_and_stubs_into_a_constructor()
        {
            // ***** ARRANGE *****
            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();
            stubAccount.PhoneNumber = "865-425-0555";
            stubAccount
                .Stub(a => a.Balance)
                .Return(1500m);  // We need Stub() because these have no public setters
            stubAccount
                .Stub(a => a.AccountNumber)
                .Return("123abc");
            
            var mockSmsSender = MockRepository.GenerateMock<ISmsSender>();
            mockSmsSender
                .Stub(s => s.Provider)
                .Return("x");
            // This expectation is exact; see the test class on constraints to be more general.
            mockSmsSender
                .Expect(s => s.Send("865-425-0555", "Account '123abc' balance: $1,500.00 (sent via x)"));

            var manager = new BankAccountManager(stubAccountRepository, mockSmsSender);

            // ***** ACT *****
            manager.SendBalanceInfoBySms(stubAccount);

            // ***** ASSERT *****
            mockSmsSender.VerifyAllExpectations();
        }

        /// <summary>
        /// Use Case:
        /// You want to verify the logic of the code under test which relies on a dependency.
        /// 
        /// Example:
        /// The BankAccountManager.GetNumberOfAccountsWithInvalidPhoneNumbers() method uses the
        /// IBankAccountRepository.GetAllBankAccounts() method so that the accounts can be 
        /// enumerated. Stubs are appropriate here because we want to make dummy accounts to
        /// be returned by IBankAccountRepository() so that we only test the logic in 
        /// GetNumberOfAccountsWithInvalidPhoneNumbers().
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.GetNumberOfAccountsWithInvalidPhoneNumbers_MixOfValidAndInvalidPhoneNumbers_ReturnsCorrectCount()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Using_stubs_to_mimic_behavior()
        {
            // ***** ARRANGE *****
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();
            
            // Make dummy accounts
            var stubAccounts = new List<IBankAccount>();
            for (int i = 0; i < 5; i++)
            {
                stubAccounts.Add(MockRepository.GenerateStub<IBankAccount>());
            }
            stubAccounts[0].PhoneNumber = "865-425-0555";
            stubAccounts[1].PhoneNumber = String.Empty;
            stubAccounts[2].PhoneNumber = "865-425-0551";
            stubAccounts[3].PhoneNumber = "8654250551";
            stubAccounts[4].PhoneNumber = "425-0551";

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetAllBankAccounts())
                .Return(stubAccounts);

            var manager = new BankAccountManager(stubAccountRepository, stubSmsSender);
            
            // ***** ACT *****
            var numInvalid = manager.GetNumberOfAccountsWithInvalidPhoneNumbers();

            // ***** ASSERT *****
            Assert.AreEqual(3, numInvalid);
        }

        /// <summary>
        /// Use Case:
        /// You want to verify the logic of the code under test responds accordingly when
        /// an exception is thrown.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() catches an exception if
        /// IBankAccountRepository.GetBankAccount() throws an exception. It then adds additional
        /// information and throws a second exception.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_GetBankAccountThrowsException_ThrowsCorrectException()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Using_a_stub_to_throw_an_exception()
        {
            // ***** ARRANGE *****
            var exceptionToThrow = new ApplicationException("Flagrant error occurred in the repository!");
            
            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Throw(exceptionToThrow);

            var manager = new BankAccountManager(stubAccountRepository, stubSmsSender);

            const string acctNumber = "123abc";

            // ***** ACT *****
            try
            {
                manager.ProcessTransactionsOnAccount(acctNumber, new List<decimal> { 0 });
            }
            catch (Exception ex)
            {
                // ***** ASSERT *****
                const string expectedMessage = "Unable to retrieve account '" + acctNumber + "'";
                Assert.AreEqual(expectedMessage, ex.Message);
                Assert.AreEqual(exceptionToThrow, ex.InnerException);
                return;
            }

            Assert.Fail("An exception should have been thrown by ProcessTransactionsOnAccount");
        }
    }
}