﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace UnitTestingAndMockingExamples.Tests.RhinoMocksExamples
{
    [TestClass]
    public class Generics
    {
        /// <summary>
        /// Use Case:
        /// You want to mock something on a interface that uses generics.
        /// 
        /// Example:
        /// The BankAccountManager.ProcessTransactionsOnAccount() method uses the IList(T).Count
        /// property to check if it has anything to process. This test verifies IList(decimal).Count
        /// is called.
        /// 
        /// Note: Essentially this shows how Rhino Mocks enforces type safety
        /// even when dealing with generics.
        /// 
        /// Alternate Test Method Name:
        /// BankAccountManagerTests.ProcessTransactionsOnAccount_GivenEmptyList_UsesListCount()
        /// </summary>
        [TestMethod]
        [TestCategory("RhinoMocks")]
        public void Mocking_a_generic_interface()
        {
            // ***** ARRANGE *****
            var stubAccount = MockRepository.GenerateStub<IBankAccount>();

            var stubAccountRepository = MockRepository.GenerateStub<IBankAccountRepository>();
            stubAccountRepository
                .Stub(r => r.GetBankAccount(Arg<string>.Is.Anything))
                .Return(stubAccount);

            var stubSmsSender = MockRepository.GenerateStub<ISmsSender>();

            var mockAmountList = MockRepository.GenerateMock<IList<decimal>>();
            mockAmountList
                .Expect(l => l.Count)
                .Return(0);

            var acctManager = new BankAccountManager(stubAccountRepository, stubSmsSender);

            // ***** ACT *****
            acctManager.ProcessTransactionsOnAccount("whatever", mockAmountList);

            // ***** ASSERT *****
            mockAmountList.VerifyAllExpectations();  
        }
    }
}
