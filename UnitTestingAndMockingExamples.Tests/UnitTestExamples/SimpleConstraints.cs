﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    /// <summary>
    /// Unit tests for dealing with basic constraints
    /// </summary>
    [TestClass]
    public class SimpleConstraints
    {
        [TestMethod]
        public void Instance_is_null()
        {
            object nada = null;
            Assert.IsNull(nada);
        }

        [TestMethod]
        public void Instance_is_not_null()
        {
            var nada = new object();
            Assert.IsNotNull(nada);
        }

        [TestMethod]
        public void Value_is_NaN()
        {
            const double d = double.NaN;
            const float f = float.NaN;

            Assert.AreEqual(double.NaN, d);
            Assert.AreEqual(float.NaN, f);
        }

        [TestMethod]
        public void Value_is_an_empty_string()
        {
            // NOTES:
            //   (1) The StringAssert class doesn't help here.
            //   (2) The IsNullOrEmpty() method is from .NET 4.0
            Assert.IsTrue(String.IsNullOrEmpty(""));
            Assert.IsFalse(String.IsNullOrEmpty("Hello world!"));
        }
    }
}
