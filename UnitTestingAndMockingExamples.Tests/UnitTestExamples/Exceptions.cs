﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    [TestClass]
    public class Exceptions
    {
        // GENERAL NOTES:
        //   (1) If an exception is thrown inside a test method that is not 
        //       expected or caught, that test will fail.
        //   (2) MS Test does not have an equivalent to NUnit's 
        //       Assert.DoesNotThrow(). You'll have to use try/catch blocks
        //       to create a workaround.

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Catch_a_specific_exception()
        {
            // NOTE: If the attribute was "[ExpectedException(typeof(Exception))]" this test would fail.

            // ReSharper disable once CollectionNeverUpdated.Local
            var numbers = new List<int>();

            // ReSharper disable UnusedVariable
            var badValue = numbers[-1];
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void Catch_a_specific_exception_using_assert_extensions()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var numbers = new List<int>();
            
            // ReSharper disable once NotAccessedVariable
            int badValue;

            AssertEx.Throws<ArgumentOutOfRangeException>(() => { badValue = numbers[-1]; });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), AllowDerivedTypes=true)]
        public void Catch_a_general_exception()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var numbers = new List<int>();

            // NOTE: This throws System.ArgumentOutOfRange, which is a subclass of System.Exception

            // ReSharper disable UnusedVariable
            var badValue = numbers[-1];
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void Catch_a_general_exception_using_assert_extensions()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var numbers = new List<int>();

            // ReSharper disable once NotAccessedVariable
            int badValue;

            AssertEx.Throws(() => { badValue = numbers[-1]; });
        }

        [TestMethod]
        public void Catch_an_exception_using_Assert()
        {
            // ReSharper disable once CollectionNeverUpdated.Local
            var numbers = new List<int>();

            // NOTE: This will simply assert that the exception was thrown
            try
            {
                // ReSharper disable UnusedVariable
                var badValue = numbers[-1];
                // ReSharper restore UnusedVariable
            }
            catch
            {
                Assert.IsTrue(true);
                return;
            }

            // If the exception was not thrown, we ended up here, so we need to force a bad assertion to make the test fail.
            Assert.IsTrue(false);
        }

        [TestMethod]
        public void Evaluating_properties_of_an_exception()
        {
            // To get at the exception properties, you have to use a try/catch
            // block (because you need access to the exception instance).
            try
            {
                throw new Exception("Special message here");
            }
            catch (Exception ex)
            {
                // This asserts that the exception was thrown.
                Assert.IsTrue(true);

                // This asserts that the message was correct.
                Assert.AreEqual(ex.Message, "Special message here");
            }
        }

        [TestMethod]
        public void Ensuring_an_exception_was_not_thrown_using_assert_extension()
        {
            // NOTE: It may be best to just let the exception be unhandled, as
            //       that will implicitly fail the test.

            var numbers = new List<int> {1};
            // ReSharper disable once NotAccessedVariable
            int goodValue;
            
            AssertEx.DoesNotThrow(() => { goodValue = numbers[0]; });
        }

        [TestMethod]
        [ExpectedException(typeof (InvalidOperationException))]
        public async Task Catching_async_exceptions()
        {
            var systemUnderTest = new DummySystemUnderTest();
            
            await systemUnderTest.FailAsync();
        }

        [TestMethod]
        public async Task Catching_async_exceptions_using_assert_extension()
        {
            var systemUnderTest = new DummySystemUnderTest();

            await AssertEx.ThrowsAsync<InvalidOperationException>(() => systemUnderTest.FailAsync());
        }
    }

    internal class DummySystemUnderTest
    {
        public async Task FailAsync()
        {
            await Task.Delay(1000);
            throw new InvalidOperationException("Epic failure occurred!");
        }
    }

}
