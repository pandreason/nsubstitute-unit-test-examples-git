﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    [TestClass]
    public class Collections
    {
        [TestMethod]
        public void All_items_are_not_null()
        {
            var ints = new object[] { 1, 2, 3, 4 };
            CollectionAssert.AllItemsAreNotNull(ints);
        }

        [TestMethod]
        public void All_items_are_of_the_same_type()
        {
            var ints = new object[] { 1, 2, 3, 4 };
            CollectionAssert.AllItemsAreInstancesOfType(ints, typeof(int));

            // NOTE: MS Test's CollectionAssert.AllItemsAreInstancesOfType() sort of 
            //       breaks the usual pattern of the first parameter being the expected 
            //       value and the second parameter being the actual value.
        }

        [TestMethod]
        public void All_items_are_unique()
        {
            var ints = new object[] { 1, 2, 3, 4 };
            CollectionAssert.AllItemsAreUnique(ints);
        }

        [TestMethod]
        public void Not_all_items_are_unique()
        {
            var strings = new object[] { "abc", "bad", "cab", "bad", "dad" };
            Assert.AreNotEqual(strings.Length, strings.Distinct().Count());

            // There is no CollectionAssert.AllItemsAreNotUnique(), so use LINQ.
        }

        [TestMethod]
        public void All_items_meet_some_condition()
        {
            var ints = new[] { 1, 2, 3, 4 };
            Assert.AreEqual(ints.Length, ints.Count(i => i > 0));


            var strings = new[] { "abc", "bad", "cab", "bad", "dad" };
            Assert.IsTrue(strings.Any(s => s.Contains("a")));

            
            Assert.IsTrue(strings.Count(s => s.Length == 3) == strings.Length);
        }

        [TestMethod]
        public void Some_items_meet_some_condition()
        {
            var mixedTypes = new object[] { 1, 2, "3", null, "four", 100 };
            Assert.IsTrue(mixedTypes.Any(o => o == null));
            Assert.IsTrue(mixedTypes.Any(o => o is int));
            Assert.IsTrue(mixedTypes.Any(o => o is string));


            var strings = new[] { "abc", "bad", "cab", "bad", "dad" };
            Assert.IsTrue(strings.Any(s => s.StartsWith("ba")));
            Assert.IsTrue(strings.Any(s => !s.StartsWith("ba")));
        }

        [TestMethod]
        public void No_items_meet_condition()
        {
            var ints = new object[] { 1, 2, 3, 4 };
            var strings = new object[] { "abc", "bad", "cab", "bad", "dad" };

            Assert.IsFalse(ints.Any(i => i is string));
            Assert.IsFalse(strings.Any(s => ((string)s).StartsWith("qu")));
            Assert.IsTrue(strings.Count(s => ((string)s).Length == 3) == strings.Length);
        }

        [TestMethod]
        public void Collection_contains_an_item()
        {
            var ints = new[] { 1, 2, 3 };
            var strings = new[] { "a", "b", "c" };

            CollectionAssert.Contains(ints, 3);
            CollectionAssert.Contains(strings, "b");
            CollectionAssert.Contains(ints, (int) 3.0d);
        }

        [TestMethod]
        public void Collection_doesnt_contain_an_item()
        {
            var strings = new[] { "a", "b", "c" };
            CollectionAssert.DoesNotContain(strings, "x");
        }

        [TestMethod]
        public void A_given_collection_is_equivalent()
        {
            var intsOneThroughFive = Enumerable.Range(1, 5).ToList();

            // See if two arrays are equivalent (same members, regardless of order).
            CollectionAssert.AreEquivalent(new[] { 2, 1, 4, 3, 5 }, intsOneThroughFive);
        }

        [TestMethod]
        public void A_given_collection_is_not_equivalent()
        {
            var intsOneThroughFive = Enumerable.Range(1, 5).ToList();

            // See if two arrays are not equivalent (different members, regardless of order).
            CollectionAssert.AreNotEquivalent(new[] { 2, 2, 4, 3, 5 }, intsOneThroughFive);
            CollectionAssert.AreNotEquivalent(new[] { 2, 4, 3, 5 }, intsOneThroughFive);
            CollectionAssert.AreNotEquivalent(new[] { 2, 2, 1, 1, 4, 3, 5 }, intsOneThroughFive);
        }

        [TestMethod]
        public void A_given_collection_is_a_subset()
        {
            var intsOneThroughFive = Enumerable.Range(1, 5).ToList();

            CollectionAssert.IsSubsetOf(new[] { 1, 3, 5 }, intsOneThroughFive);
            CollectionAssert.IsSubsetOf(new[] { 1, 2, 3, 4, 5 }, intsOneThroughFive);
        }

        [TestMethod]
        public void A_given_collection_is_not_a_subset()
        {
            var intsOneThroughFive = Enumerable.Range(1, 5).ToList();

            CollectionAssert.IsNotSubsetOf(new[] { 2, 4, 6 }, intsOneThroughFive);
        }

        [TestMethod]
        public void Two_equivalent_collections_have_similar_types()
        {
            var integers = new[] { 1, 2, 3 };
            var doubles = new[] { 1.0, 2.0, 3.0 };

            // NOTE: Yes, I know this is ridiculous, but Assert and CollectionAssert
            // are of no help here. With CollectionAssert, all the items in both
            // collections need to be the same type. (Note: Double is more precise,
            // so I used that instead of Single.)
            var expectedEnum = integers.GetEnumerator();
            var actualEnum = doubles.GetEnumerator();
            int count;
            for (count = 0; expectedEnum.MoveNext() && actualEnum.MoveNext(); count++)
                Assert.AreEqual(Convert.ToDouble(expectedEnum.Current), Convert.ToDouble(actualEnum.Current));
            Assert.IsTrue(count == integers.Length && count == doubles.Length);
        }

        [TestMethod]
        public void Two_collections_are_not_equal()
        {
            var integers = new[] { 1, 2, 3 };
            var integersInDifferentOrder = new[] { 1, 3, 2 };

            // NOTE: The two collections here are "equivalent", meaning they
            // have the same values. They are not equal because the order is
            // not the same.
            CollectionAssert.AreNotEqual(integers, integersInDifferentOrder);
        }

        [TestMethod]
        public void Collection_is_empty()
        {
            // NOTES: 
            //   (1) The CollectionAssert class doesn't help here. If you try to
            //       call CollectionAssert.AllItemsAreNotNull(bArray) on an empty
            //       array, the assertion will pass.
            //   (2) The Count() extension method works with any IEnumerable instance.
            var bArray = new bool[0];
            Assert.IsTrue(bArray.Count() == 0);

            var iList = new List<int> { 1, 2, 3 };
            Assert.IsTrue(iList.Count() != 0);
        }

        [TestMethod]
        public void Collection_is_empty_or_not_using_assert_extensions()
        {
            var bArray = new bool[0];
            AssertEx.IsEmpty(bArray);

            var iList = new List<int> { 1, 2, 3 };
            AssertEx.IsNotEmpty(iList);
        }
    }
}
