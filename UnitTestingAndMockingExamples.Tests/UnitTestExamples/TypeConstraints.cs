﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    /// <summary>
    /// Unit tests for type constraints
    /// </summary>
    [TestClass]
    public class TypeConstraints
    {
        [TestMethod]
        public void Instance_is_a_certain_type()
        {
            Assert.AreEqual(typeof(string), "Hello world!".GetType());

            var iList = new List<int> { 1, 2, 3 };
            Assert.IsInstanceOfType(iList, typeof(IEnumerable<int>));
        }

        [TestMethod]
        public void Instance_is_not_a_certain_type()
        {
            Assert.AreNotEqual(typeof(string), 5.GetType());

            Assert.IsNotInstanceOfType("Hello", typeof(IEnumerable<string>));
        }

        [TestMethod]
        public void Instance_is_assignable_from_a_type()
        {
            // See if something can be assigned an instance of/from something else
            // For example, 's' can be assigned an instance of/from System.String
            const string s = "Hello world!";
            Assert.IsTrue(s.GetType().IsAssignableFrom(typeof(String)));
        }

        [TestMethod]
        public void Instance_is_not_assignable_from_a_type()
        {
            // See if something cannot be assigned an instance of/from something else
            // For example, 'i' cannot be assigned an instance of/from System.String
            const int i = 42;
            Assert.IsFalse(i.GetType().IsAssignableFrom(typeof(String)));
        }
    }
}
