﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    /// <summary>
    /// Source: Eric Dietrich (https://gist.github.com/erikdietrich/4041266)
    /// </summary>
    public static class AssertEx
    {
        /// <summary>Check that a statement throws a specific type of exception</summary>
        /// <typeparam name="TException">Exception type inheriting from Exception</typeparam>
        /// <param name="executable">Block that should throw the exception</param>
        public static void Throws<TException>(Action executable) where TException : Exception
        {
            try
            {
                executable();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(TException), String.Format("Expected exception of type {0} but got {1}", typeof(TException), ex.GetType()));
                return;
            }
            // ReSharper disable once RedundantStringFormatCall
            Assert.Fail(String.Format("Expected exception of type {0}, but no exception was thrown.", typeof(TException)));
        }

        /// <summary>Check that a statement throws some kind of exception</summary>
        /// <param name="executable">Block that should throw the exception</param>
        /// <param name="message">Failure message to display</param>
        public static void Throws(Action executable, string message = "Expected an exception but none was thrown.")
        {
            try
            {
                executable();
            }
            catch
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.Fail(message);
        }

        /// <summary>Check that a statement does not throw an exception</summary>
        /// <param name="executable">Action to execute</param>
        public static void DoesNotThrow(Action executable)
        {
            try
            {
                executable();
            }
            catch (Exception ex)
            {
                // ReSharper disable once RedundantStringFormatCall
                Assert.Fail(String.Format("Expected no exception, but exception of type {0} was thrown.", ex.GetType()));
            }
        }

        /// <summary>Check that a collection is empty</summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">The collection</param>
        public static void IsEmpty<T>(ICollection<T> collection)
        {
            Assert.IsTrue(collection.Count == 0, "Empty collection expected, but actual count is " + collection.Count);
        }

        /// <summary>Check that a list/collection is not empty</summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">Collection in question</param>
        public static void IsNotEmpty<T>(ICollection<T> collection)
        {
            Assert.IsFalse(collection.Count == 0, "Non-empty collection expected, but collection is empty.");
        }

        /// <summary>Helps determine if a given asynchronous statement throws an exception
        /// Derived from -- http://msdn.microsoft.com/en-us/magazine/dn818493.aspx
        /// </summary>
        /// <typeparam name="TException">Exception type inheriting from Exception</typeparam>
        /// <param name="executable">Block that should throw the exception</param>
        /// <param name="allowDerivedTypes">True if exceptions derived from TException are allows</param>
        /// <returns>Exception caught, or an exception with reasons why the exception wasn't caught</returns>
        public static async Task<TException> ThrowsAsync<TException>(Func<Task> executable, bool allowDerivedTypes = true) where TException : Exception
        {
            try
            {
                await executable();
            }
            catch (Exception ex)
            {
                if (allowDerivedTypes && !(ex is TException))
                    throw new Exception("Delegate threw exception of type " + ex.GetType().Name + ", but " + typeof(TException).Name + " or a derived type was expected.", ex);
                if (!allowDerivedTypes && ex.GetType() != typeof(TException))
                    throw new Exception("Delegate threw exception of type " + ex.GetType().Name + ", but " + typeof(TException).Name + " was expected.", ex);
                return (TException) ex;
            }

            throw new Exception("Delegate did not throw expected exception " + typeof(TException).Name + ".");
        }

        /// <summary>Helps determine if the given asynchronous statement throws an exception of any kind
        /// Derived from -- http://msdn.microsoft.com/en-us/magazine/dn818493.aspx
        /// </summary>
        /// <param name="executable">Block that should throw the exception</param>
        /// <returns>Exception caught, or an exception with reasons why the exception wasn't caught</returns>
        public static Task<Exception> ThrowsAsync(Func<Task> executable)
        {
            return ThrowsAsync<Exception>(executable, true);
        }
    }
}
