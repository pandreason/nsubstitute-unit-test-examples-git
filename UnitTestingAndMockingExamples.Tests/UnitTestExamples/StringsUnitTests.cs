﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    /// <summary>
    /// Unit tests for dealing with strings
    /// </summary>
    [TestClass]
    public class StringsUnitTests
    {
        #region Contains/doesn't contain

        [TestMethod]
        public void String_contains_a_given_substring()
        {
            StringAssert.Contains("Hello world!", "world");

            // NOTES: 
            //   (1) MS Test's StringAssert.Contains() sort of breaks the usual
            //       pattern of the first parameter being the expected value and
            //       the second parameter being the actual value.
            //   (2) The assertion is case sensitive.      
        }

        [TestMethod]
        public void String_doesnt_contain_substring()
        {
            // NOTES:
            //   (1) I don't know why there's no StringAssert.DoesNotContain().
            //   (2) The String.Contains() method is case sensitive.      
            Assert.IsFalse("Hello world!".Contains("goodbye"));
        }

        [TestMethod]
        public void String_contains_a_given_substring_case_insensitive()
        {
            // NOTE: We can't use String.Contains() here because none of the
            //       overloads for that method deal with case insensitivity.
            //       Instead we have to use a workaround to find the index
            //       of the given substring in the target string. If that
            //       substring is found, it will have non-negative index.
            Assert.IsTrue("Hello world!".IndexOf("WORLD", StringComparison.InvariantCultureIgnoreCase) >= 0);
        }

        [TestMethod]
        public void String_doesnt_contain_a_given_substring_case_insensitive()
        {
            // NOTES: 
            //   (1) We can't use String.Contains() here because none of the
            //       overloads for that method deal with case insensitivity.
            //       Instead we have to use a workaround to find the index
            //       of the given substring in the target string. If that
            //       substring is found, it will have non-negative index.
            //   (2) You can use Assert.IsTrue(<index value> < 0), but I've been
            //       using the convention of using Assert.IsFalse() for 
            //       "not"-type operations.
            Assert.IsFalse("Hello world!".IndexOf("BYE", StringComparison.InvariantCultureIgnoreCase) >= 0);
        }

        [TestMethod]
        public void String_array_contains_a_given_substring_case_insensitive()
        {
            var stringArray = new[] { "abc", "bad", "dba" };

            // NOTES:
            //   (1) CollectionAssert and StringAssert don't help here because you 
            //       can only make assertions about the entire collection (not each
            //       of its members) and StringAssert only works on individual strings.
            //   (2) You could use a foreach loop to iterate over the collection, then
            //       use StringAssert. However, if you wanted to use case 
            //       insensitivity, StringAssert won't be of any use.

            // (You could do the following with a foreach, but I chose to use LINQ.
            // Basically, you're counting the number of strings 's' in 'sArray' that
            // contain "b". For the assertion to work, the number of strings in the
            // array that contain "b" should match the number of strings in the 
            // entire array.)
            Assert.AreEqual(stringArray.Length, stringArray.Count(s => s.IndexOf("b", StringComparison.InvariantCultureIgnoreCase) >= 0));
        }

        #endregion

        #region Starts/doesn't start with
        
        [TestMethod]
        public void String_starts_with()
        {
            // NOTES: 
            //   (1) MS Test's StringAssert.StartsWith() sort of breaks the usual
            //       pattern of the first parameter being the expected value and
            //       the second parameter being the actual value.
            //   (2) The assertion is case sensitive.  
            StringAssert.StartsWith("Hello world!", "Hello");
        }

        [TestMethod]
        public void String_doesnt_start_with()
        {
            // NOTES:
            //   (1) I don't know why there's no StringAssert.DoesNotStartWith().
            //   (2) The String.StartsWith() method is case sensitive. 
            Assert.IsFalse("Hello world!".StartsWith("Hi!"));

            // NUnit: Assert.That(phrase, Text.DoesNotStartWith("Hi!"))
        }

        [TestMethod]
        public void String_starts_with_case_insensitive()
        {
            Assert.IsTrue("Hello world!".StartsWith("HeLLo", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void String_doesnt_start_with_case_insensitive()
        {
            Assert.IsFalse("Hello world!".StartsWith("HI", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void String_array_starts_with_case_insensitive()
        {
            var stringArray = new[] { "Hello!", "hi!", "Hola!" };

            // NOTES:
            //   (1) CollectionAssert and StringAssert don't help here because you 
            //       can only make assertions about the entire collection (not each
            //       of its members) and StringAssert only works on individual strings.
            //   (2) You could use a foreach loop to iterate over the collection, then
            //       use StringAssert. However, if you wanted to use case 
            //       insensitivity, StringAssert won't be of any use.

            // (You could do the following with a foreach, but I chose to use LINQ.
            // Basically, you're counting the number of strings 's' in 'sArray' that
            // start with "h" (ignoring case). For the assertion to work, the number 
            // of strings in the array that start with "h" should match the number of 
            // strings in the entire array.)
            Assert.AreEqual(stringArray.Length, stringArray.Count(s => s.StartsWith("h", StringComparison.InvariantCultureIgnoreCase)));
        }

        #endregion

        #region Ends/doesn't end with

        [TestMethod]
        public void String_ends_with()
        {
            // NOTES: 
            //   (1) MS Test's StringAssert.EndsWith() sort of breaks the usual
            //       pattern of the first parameter being the expected value and
            //       the second parameter being the actual value.
            //   (2) The assertion is case sensitive. 
            StringAssert.EndsWith("Hello world!", "!");
        }

        [TestMethod]
        public void String_doesnt_end_with()
        {
            // NOTES:
            //   (1) I don't know why there's no StringAssert.DoesNotEndWith().
            //   (2) The String.EndsWith() method is case sensitive. 
            Assert.IsFalse("Hello world!".EndsWith("?"));
        }

        [TestMethod]
        public void String_ends_with_case_insensitive()
        {
            Assert.IsTrue("Hello world!".EndsWith("WORLD!", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void String_doesnt_end_with_case_insensitive()
        {
            Assert.IsFalse("Hello world!".EndsWith("everybody", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void String_array_ends_with_case_insensitive()
        {
            var stringArray = new[] { "Hello!", "hi!", "Hola!" };

            // NOTES:
            //   (1) CollectionAssert and StringAssert don't help here because you 
            //       can only make assertions about the entire collection (not each
            //       of its members) and StringAssert only works on individual strings.
            //   (2) You could use a foreach loop to iterate over the collection, then
            //       use StringAssert. However, if you wanted to use case 
            //       insensitivity, StringAssert won't be of any use.

            // (You could do the following with a foreach, but I chose to use LINQ.
            // Basically, you're counting the number of strings 's' in 'sArray' that
            // end with "!" (ignoring case). For the assertion to work, the number of 
            // strings in the array that end with "!" should match the number of 
            // strings in the entire array.)
            Assert.IsTrue(stringArray.Count(s => s.EndsWith("!", StringComparison.InvariantCultureIgnoreCase)) == stringArray.Length);
        }

        #endregion

        #region String equality

        [TestMethod]
        public void Strings_are_equal_case_insensitive()
        {
            const bool ignoreCase = true;
            Assert.AreEqual("hello world!", "Hello world!", ignoreCase);
        }

        [TestMethod]
        public void Strings_are_not_equal_case_insensitive()
        {
            const bool ignoreCase = true;
            Assert.AreNotEqual("goodbye world!", "Hello world!", ignoreCase);
        }

        [TestMethod]
        public void Array_of_strings_are_equal_case_insensitive()
        {
            var pascalCaseStrings = new[] { "Hello", "World" };
            var uppercaseStrings = new[] { "HELLO", "WORLD" };
            const bool ignoreCase = true;

            // NOTE: CollectionAssert.AreEquivalent() won't work here because
            //       it doesn't support case insensitivity.
            Assert.AreEqual(pascalCaseStrings.Length, uppercaseStrings.Length);
            for (var i = 0; i < pascalCaseStrings.Length; i++)
                Assert.AreEqual(pascalCaseStrings[i], uppercaseStrings[i], ignoreCase);
        }

        [TestMethod]
        public void Array_of_strings_are_not_equal_case_insensitive()
        {
            var hellos = new[] { "HELLO", "Hello", "hello" };
            const bool ignoreCase = true;

            // NOTE: There's not a method in CollectionAssert that will be useful
            //       for testing this condition.
            foreach (var s in hellos)
                Assert.AreEqual(s, "hello", ignoreCase);
        }

        #endregion

        #region Regular expressions

        [TestMethod]
        public void String_matches_a_regex()
        {
            const string phrase = "Now is the time for all good men to come to the aid of their country.";

            // NOTE: MS Test's StringAssert.Matches() breaks the usual pattern
            // of the first parameter being the expected value and the second 
            // parameter being the actual value.
            StringAssert.Matches(phrase, new Regex("all good men"));
            StringAssert.Matches(phrase, new Regex("Now.*come"));
            
            // Here's an example that ignores case...
            StringAssert.Matches(phrase, new Regex("ALL", RegexOptions.IgnoreCase));
        }

        [TestMethod]
        public void String_doesnt_match_a_regex()
        {
            const string phrase = "Now is the time for all good men to come to the aid of their country.";

            // NOTE: MS Test's StringAssert.DoesNotMatch() breaks the usual pattern 
            // of the first parameter being the expected value and the second 
            // parameter being the actual value.
            StringAssert.DoesNotMatch(phrase, new Regex("all.*men.*good"));
        }

        [TestMethod]
        public void Array_of_strings_match_a_regex()
        {
            var quotes = new[] { "Never say never", "It's never too late", "Nevermore!" };

            // NOTES:
            //   (1) CollectionAssert and StringAssert don't help here because you can only
            //       make assertions about the entire collection (not each of its members) and
            //       StringAssert only works on individual strings.
            //   (2) You could use a foreach loop to iterate over the collection, then use 
            //       StringAssert. However, if you wanted to use case insensitivity, 
            //       StringAssert won't be of any use.

            // You could do the following with a foreach, but I chose to use LINQ.
            // Basically, you're counting the number of times "never" matches the 
            // in string 's' from 'quotes' more than zero times. For the assertion 
            // to work, the number of strings in the array that regex-match "never"
            // at least once should be equal to the number of strings in the entire
            // array.
            Assert.IsTrue(quotes.Count(s => Regex.Matches(s, "never", RegexOptions.IgnoreCase).Count > 0) == quotes.Length);
        }

        #endregion
    }
}
