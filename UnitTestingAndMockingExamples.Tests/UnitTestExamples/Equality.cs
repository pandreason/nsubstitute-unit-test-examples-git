﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestingAndMockingExamples.Tests.UnitTestExamples
{
    /// <summary>
    /// Unit tests for dealing with equality
    /// </summary>
    [TestClass]
    public class Equality
    {
        [TestMethod]
        public void Basic_equality()
        {
            Assert.AreEqual(4, 2 + 2);
        }

        [TestMethod]
        public void Basic_inequality()
        {
            Assert.AreNotEqual(5, 2 + 2);
        }

        [TestMethod]
        public void Equality_with_tolerance()
        {
            // See if two values are within a given amount of each other.
            // (In these cases, we're comparing doubles with doubles and floats with floats.)
            Assert.AreEqual(499, 500, 5);
            Assert.AreEqual(5.0d, 4.99d, 0.05d);
            Assert.AreEqual(5.0f, 4.99f, 0.05f);
        }

        [TestMethod]
        public void Equality_with_tolerance_for_decimals()
        {
            const decimal expected = 5.0m;
            const decimal observed = 4.99m;
            const decimal tolerance = 0.05m;

            // NOTE: There is no MS Test method to compare decimals within tolerance, so
            // you have to use a workaround.
            Assert.IsTrue(Math.Abs(observed - expected) <= tolerance);
        }

        [TestMethod]
        public void Equality_with_tolerance_for_unsigned_ints()
        {
            const uint expected = 3999999999u;
            const uint observed = 4000000000u;
            const uint tolerance = 5u;

            // NOTE: There is no MS Test method to compare uints within tolerance, so
            // you have to use a workaround.
            Assert.IsTrue(Math.Abs(observed - expected) <= tolerance);
        }

        [TestMethod]
        public void Inequality_with_tolerance()
        {
            // See if two values are not within a given amount of each other.
            // (In these cases, we're comparing doubles with doubles and floats with floats.)
            Assert.AreNotEqual(4.0d, 5.0d, 0.5d);
            Assert.AreNotEqual(4.0f, 5.0f, 0.5f);
        }

        [TestMethod]
        public void Equality_of_reference_types()
        {
            var firstList = new List<int> { 1, 2, 3 };
            var secondList = new List<int> { 1, 2, 3 };

            // Assert.AreNotEqual() compares *references*, not content.
            Assert.AreNotEqual(firstList, secondList);

            // This compares the content of the lists.
            CollectionAssert.AreEqual(firstList.ToList(), secondList.ToList());
        }
    }
}
